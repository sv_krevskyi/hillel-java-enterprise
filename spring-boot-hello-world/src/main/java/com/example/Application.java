package com.example;

import com.example.service.HelloWorldService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Arrays;

@SpringBootApplication
public class Application {
    private static Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) throws Exception, BeansException {

        ConfigurableApplicationContext ctx = SpringApplication.run(Application.class, args);

        logger.info("The beans you were looking for:");

        // listing all bean definition names
        Arrays.stream(ctx.getBeanDefinitionNames()).forEach(logger::info);

        HelloWorldService hw = ctx.getBean(HelloWorldService.class);
        hw.sayHi();
        System.in.read();
        ctx.close();
    }
}
