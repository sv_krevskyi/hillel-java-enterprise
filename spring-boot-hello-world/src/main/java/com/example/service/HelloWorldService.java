package com.example.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class HelloWorldService {
    private static Logger logger = LoggerFactory.getLogger(HelloWorldService.class);

    public void sayHi( ) {
        logger.info("Hello World!");
    }
}
