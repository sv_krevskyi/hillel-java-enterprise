package basic.service;

import basic.db.dao.impl.VehicleDao;
import basic.db.entity.Manufacturer;
import basic.db.entity.Vehicle;

import java.util.List;

public class VehicleService {
    private final VehicleDao vehicleDao;

    public VehicleService(VehicleDao vehicleDao) {
        this.vehicleDao = vehicleDao;
    }

    public Vehicle createVehicle(Vehicle vehicle) {
        return vehicleDao.create(vehicle);
    }

    public void createVehicles(List<Vehicle> vehicles) {
        vehicleDao.create(vehicles);
    }

    public Vehicle getVehicleById(long id) {
        return vehicleDao.get(id);
    }

    public List<Vehicle> getAll() {
        return vehicleDao.getAll();
    }

    public boolean update(Vehicle vehicle) {
        return vehicleDao.update(vehicle);
    }

    public void assignManufacturerToVehicle(Manufacturer manufacturer, Vehicle vehicle) {
        //todo
    }

    public boolean delete(Vehicle vehicle) {
        return vehicleDao.update(vehicle);
    }

}
