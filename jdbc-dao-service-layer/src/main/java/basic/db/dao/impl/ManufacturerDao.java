package basic.db.dao.impl;

import basic.db.dao.BaseDao;
import basic.db.entity.Manufacturer;
import basic.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static basic.db.dao.impl.ManufacturerDao.Table.ID;
import static basic.db.dao.impl.ManufacturerDao.Table.INSERT_SQL;
import static basic.db.dao.impl.ManufacturerDao.Table.NAME;
import static basic.db.dao.impl.ManufacturerDao.Table.SELECT_ONE_SQL;
import static basic.db.dao.impl.ManufacturerDao.Table.DELETE_SQL;
import static basic.db.dao.impl.ManufacturerDao.Table.SELECT_ALL_SQL;
import static basic.db.dao.impl.ManufacturerDao.Table.UPDATE_SQL;


public class ManufacturerDao implements BaseDao<Manufacturer> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ManufacturerDao.class);
    private static final String MAIN_EXCEPTION_MESSAGE = "Exception occurred";

    private DataSource dataSource;

    public ManufacturerDao(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Manufacturer create(Manufacturer manufacturer) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(INSERT_SQL, Statement.RETURN_GENERATED_KEYS)) {

            ps.setString(1, manufacturer.getName());

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new DAOException("Error creating manufacturer");
            }
            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {

                if (generatedKeys.next()) {
                    final long id = generatedKeys.getLong(1);
                    return new Manufacturer(id, manufacturer);

                } else {
                    throw new DAOException("Could not create manufacturer");
                }
            }
        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public void create(List<Manufacturer> list) {
        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false); //true

            ps = conn.prepareStatement(INSERT_SQL, new String[]{ID});

            for (int i = 0; i < list.size(); i++) {

                ps.setString(1, list.get(i).getName());
                ps.addBatch();
            }
            ps.executeBatch();
            conn.commit();

            LOGGER.info("Prepared statement batch insert successfully");

        } catch (SQLException e) {
            try {
                conn.rollback();
            } catch (SQLException ex) {
                LOGGER.error("Error during rollback ", ex);
            }
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                LOGGER.error("Error during closing connection", e);
            }
        }
    }

    @Override
    public Manufacturer get(long id) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(SELECT_ONE_SQL)) {
            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return toManufacturer(rs);
                } else {
                    return null;
                }
            }
        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public List<Manufacturer> getAll() {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement ps = conn.prepareStatement(SELECT_ALL_SQL);
             ResultSet rs = ps.executeQuery()) {

            final List<Manufacturer> manufacturers = new ArrayList<>();
            while (rs.next()) {
                manufacturers.add(toManufacturer(rs));
            }
            return manufacturers;
        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public boolean update(Manufacturer manufacturer) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(UPDATE_SQL)) {

            ps.setString(1, manufacturer.getName());
            ps.setLong(2, manufacturer.getId());

            final int rowCount = ps.executeUpdate();
            return rowCount != 0;

        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public boolean delete(long id) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(DELETE_SQL)) {
            ps.setLong(1, id);

            final int rowCount = ps.executeUpdate();
            return rowCount != 0;

        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }


    private Manufacturer toManufacturer(ResultSet rs) throws SQLException {
        return Manufacturer.builder()
                .id(rs.getLong(ID))
                .name(rs.getString(NAME))
                .build();
    }


    interface Table {
        //table
        String ID = "ID";
        String NAME = "NAME";

        //scripts
        String INSERT_SQL = "INSERT INTO MANUFACTURER (NAME) VALUES (?)";
        String UPDATE_SQL = "UPDATE MANUFACTURER SET NAME=? WHERE ID = ?";
        String SELECT_ALL_SQL = "SELECT * FROM MANUFACTURER";
        String SELECT_ONE_SQL = "SELECT * FROM MANUFACTURER WHERE ID = ?";
        String DELETE_SQL = "DELETE FROM MANUFACTURER WHERE ID = ?";
    }
}
