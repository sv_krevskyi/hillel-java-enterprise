package basic;


import basic.db.connection.DataSourceFactory;
import basic.db.dao.impl.ManufacturerDao;
import basic.db.dao.impl.VehicleDao;
import basic.db.entity.Manufacturer;
import basic.db.entity.Vehicle;
import basic.service.ManufacturerService;
import basic.service.VehicleService;
import org.h2.tools.RunScript;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.SQLException;
import java.util.List;

public class Main {

    public static void main(String[] args) throws SQLException, FileNotFoundException {
        DataSource H2DataSource = DataSourceFactory.H2.configuration.configure();

        RunScript.execute(H2DataSource.getConnection(),
                new FileReader("jdbc-dao-service-layer/src/main/resources/create_db.sql")); //init database for testing purposes

        ManufacturerDao manufacturerDao = new ManufacturerDao(H2DataSource);
        VehicleDao vehicleDao = new VehicleDao(H2DataSource);

        VehicleService vehicleService = new VehicleService(vehicleDao);

        ManufacturerService manufacturerService = new ManufacturerService(manufacturerDao, vehicleDao);

        Manufacturer manufacturer = Manufacturer.builder().name("BMW").build();

        Vehicle vehicle1 = Vehicle.builder().color("white").seats(2).wheels(4).build();
        Vehicle vehicle2 = Vehicle.builder().color("red").seats(4).wheels(4).build();
        Vehicle vehicle3 = Vehicle.builder().color("green").seats(4).wheels(4).build();
        Vehicle vehicle4 = Vehicle.builder().color("blue").seats(2).wheels(2).build();

        List<Vehicle> vehiclesToCreate = List.of(vehicle1, vehicle2, vehicle3, vehicle4);

        manufacturerService.createManufacturer(manufacturer, vehiclesToCreate);

        List<Manufacturer> allManufacturers = manufacturerService.getAll();
        System.out.println(allManufacturers);

        List<Vehicle> vehicleList = vehicleService.getAll();
        System.out.println(vehicleList);
    }
}

