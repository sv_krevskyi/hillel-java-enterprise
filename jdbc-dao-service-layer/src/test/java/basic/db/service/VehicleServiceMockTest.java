package basic.db.service;

import basic.db.dao.impl.VehicleDao;
import basic.db.entity.Vehicle;
import basic.service.VehicleService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class VehicleServiceMockTest {

    private static final VehicleDao vehicleDaoMock = mock(VehicleDao.class);

    private static final long TEST_ID = 1L;
    private static final Vehicle TEST_VEHICLE_EXPECTED = new Vehicle(TEST_ID, "Red", 4, 4);

    private VehicleService vehicleService = new VehicleService(vehicleDaoMock);

    @Test
    void whenCreateVehicleShouldCreateOne() {
        //given

        when(vehicleDaoMock.create(buildTestVehicle())).thenReturn(TEST_VEHICLE_EXPECTED);

        //when
        Vehicle actual = vehicleService.createVehicle(buildTestVehicle());

        //then
        assertEquals(TEST_VEHICLE_EXPECTED, actual);
    }

    private Vehicle buildTestVehicle() {
        return Vehicle.builder().color("Red").seats(4).wheels(4).build();
    }

}
