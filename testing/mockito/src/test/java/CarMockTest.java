import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

public class CarMockTest {

    private Car car; //real object

    private Engine mockEngine;
    private FuelTank mockFuelTank;

    @BeforeEach
    public void setup() {
        mockEngine = Mockito.mock(Engine.class);
        mockFuelTank = Mockito.mock(FuelTank.class);

        car = new Car(mockEngine, mockFuelTank);
    }

    @Test
    public void whenEngineStateDiffersShouldReflectOnCarTest() {
        when(mockEngine.isRunning()).thenReturn(true);
        assertTrue(car.isEngineRunning());

        when(mockEngine.isRunning()).thenReturn(false);
        assertFalse(car.isEngineRunning());
    }

    @Test
    public void whenEngineAndFuelOkCarShouldBeStartedTest() {
        when(mockEngine.isRunning()).thenReturn(false, true);
        when(mockFuelTank.getFuel()).thenReturn(100);

        car.start();

        assertTrue(car.isCarStarted());
    }

    @Test
    public void whenNoFuelShouldBeExceptionTest() {
        when(mockEngine.isRunning()).thenReturn(false);
        when(mockFuelTank.getFuel()).thenReturn(0);

        var expectedException = assertThrows(IllegalStateException.class, () -> car.start());

        assertEquals("Can't start: no fuel", expectedException.getMessage());
    }

    @Test
    public void whenAlreadyRunningShouldBeExceptionTest() {
        when(mockFuelTank.getFuel()).thenReturn(100);
        when(mockEngine.isRunning()).thenReturn(true);

        var expectedException = assertThrows(IllegalStateException.class, () -> car.start());
        assertEquals("Engine already running", expectedException.getMessage());
    }

    @Test
    public void whenEngineNotRunningShouldBeExceptionTest() {
        when(mockEngine.isRunning()).thenReturn(false, false);
        when(mockFuelTank.getFuel()).thenReturn(100);

        var expectedException = assertThrows(IllegalStateException.class, () -> car.start());
        assertEquals("Started engine but isn't running", expectedException.getMessage());
    }
}