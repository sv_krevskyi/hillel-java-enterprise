package com.example.web.controller.integration;

import com.example.data.dao.impl.ManufacturerDao;
import com.example.data.entity.Manufacturer;
import com.example.web.controller.ControllerBaseTest;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.util.List;
import java.util.Objects;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ManufacturerControllerIntegrationTest extends ControllerBaseTest {

    @Autowired
    private ManufacturerDao manufacturerDao;

    @Test
    void whenGetAllManufacturersAndListIsEmptyThenRespondOkTest( ) throws Exception {

        mockMvc.perform(get("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(status().isOk());
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO MANUFACTURER" +
                    "(ID, ADDRESS, CAR_MODEL_NAME, COMPANY_NAME, FOUNDATION_YEAR)" +
                    " VALUES (1, 'Berlin', 'BMW', 'BMW', 1904)")
    })
    @DisplayName("should return 'manufacturer with id 1 and HTTP 200'")
    void whenListOfManufacturersLstShouldRespondOkAndReturnListTest( ) throws Exception {
        mockMvc.perform(get("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].address", is("Berlin")))
                .andExpect(jsonPath("$[0].companyName", is("BMW")))
                .andExpect(jsonPath("$[0].carModelName", is("BMW")))
                .andExpect(jsonPath("$[0].foundationYear", is(1904)))
                .andExpect(jsonPath("$[0].id", is(1))); //why not 1L ?
    }

    @Test
    @DisplayName("should return 'HTTP 201'")
    void whenCreateManufacturerShouldReturnCreatedTest( ) throws Exception {
        Manufacturer expected = Manufacturer.builder()
                .address("Detroit")
                .companyName("Ford")
                .carModelName("Ford")
                .foundationYear(1903)
                .id(1L)
                .build();

        mockMvc.perform(post("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(expected)))
                .andExpect(status().isCreated());

        List<Manufacturer> allManufacturers = manufacturerDao.findAll();

        assertTrue(allManufacturers.size() == 1);
        Manufacturer actual = allManufacturers.get(0);
        assertEquals(expected.getAddress(), actual.getAddress());
        assertEquals(expected.getCompanyName(), actual.getCompanyName());
        assertEquals(expected.getCarModelName(), actual.getCarModelName());
        assertEquals(expected.getFoundationYear(), actual.getFoundationYear());

        assertTrue(Objects.nonNull(actual.getId()) && actual.getId() > 0);

    }
}
