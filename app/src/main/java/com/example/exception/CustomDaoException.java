package com.example.exception;

public class CustomDaoException extends ApplicationGlobalException {
    public CustomDaoException() {
    }

    public CustomDaoException(String message) {
        super(message);
    }

    public CustomDaoException(String message, Throwable cause) {
        super(message, cause);
    }
}
