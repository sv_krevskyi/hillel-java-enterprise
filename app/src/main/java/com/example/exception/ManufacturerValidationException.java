package com.example.exception;

public class ManufacturerValidationException extends ApplicationGlobalException {
    public ManufacturerValidationException() {
    }

    public ManufacturerValidationException(String message) {
        super(message);
    }

    public ManufacturerValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
