package com.example.exception;

import com.example.exception.ApplicationGlobalException;

public class VehicleValidationException extends ApplicationGlobalException {
    public VehicleValidationException() {
    }

    public VehicleValidationException(String message) {
        super(message);
    }

    public VehicleValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
