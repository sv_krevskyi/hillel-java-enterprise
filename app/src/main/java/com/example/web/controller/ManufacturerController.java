package com.example.web.controller;

import com.example.data.entity.Manufacturer;
import com.example.data.service.ManufacturerService;
import com.example.exception.CustomDaoException;
import com.example.web.validation.ManufacturerValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.jpa.JpaSystemException;
import org.springframework.web.bind.annotation.*;

import javax.persistence.PersistenceException;
import java.util.List;

@RestController
@RequestMapping("/manufacturers")
public class ManufacturerController {

    private final ManufacturerService manufacturerService;

    @Autowired
    public ManufacturerController(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@RequestBody Manufacturer manufacturer) {
        ManufacturerValidator.validate(manufacturer);
        try {
            manufacturerService.save(manufacturer);
        } catch (JpaSystemException ex) {
            throw new CustomDaoException("Controller mine", ex);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Manufacturer>> getAll() {
        return new ResponseEntity<>(manufacturerService.findAll(), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@PathVariable Long id, @RequestBody Manufacturer manufacturer) {
        ManufacturerValidator.validate(manufacturer);
        manufacturerService.updateById(id, manufacturer);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        manufacturerService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

//    @ExceptionHandler(JpaSystemException.class)
//    public CustomDaoException handleDaoException(JpaSystemException exception) {
//        return new CustomDaoException("CustomDaoException Occurred", exception);
//    }
}
