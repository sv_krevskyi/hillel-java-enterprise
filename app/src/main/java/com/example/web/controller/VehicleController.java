package com.example.web.controller;

import com.example.data.entity.Vehicle;
import com.example.data.service.VehicleService;
import com.example.exception.CustomDaoException;
import com.example.web.dto.VehicleDto;
import com.example.web.validation.VehicleValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.PersistenceException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/vehicles")
@Slf4j
public class VehicleController {

    private final VehicleService vehicleService;

    @Autowired
    public VehicleController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @PostMapping(value = "/manufacturer/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> create(@PathVariable Long id, @RequestBody Vehicle vehicle) {
        VehicleValidator.validate(vehicle);
        vehicleService.createVehicleForManufacturer(id, vehicle);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VehicleDto>> getAllVehicles() {
        List<VehicleDto> result = vehicleService.getAllVehicles().stream()
                .map(VehicleDto::from)
                .collect(Collectors.toList());
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> update(@PathVariable Long id, @RequestBody Vehicle vehicle) {
        VehicleValidator.validate(vehicle);
        vehicleService.updateById(id, vehicle);
        return new ResponseEntity<>(HttpStatus.OK);//NO_CONTENT
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        vehicleService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);//NO_CONTENT
    }

    @ExceptionHandler(PersistenceException.class)
    public CustomDaoException handleDaoException(PersistenceException exception) {
        return new CustomDaoException("CustomDaoException Occurred", exception);
    }

}
