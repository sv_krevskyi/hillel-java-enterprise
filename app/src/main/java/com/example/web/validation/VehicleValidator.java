package com.example.web.validation;

import com.example.data.entity.Vehicle;
import com.example.exception.VehicleValidationException;
import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VehicleValidator {

    private static final String EMPTY_PROPERTY_EXCEPTION_MESSAGE = "Vehicle field parameter '%s' must be provided";
    private static final String REGEX_EXCEPTION_MESSAGE = "Vehicle field parameter '%s' must match these parameters: '%s'";

    private static String VALID_ENGINE_CAPACITY_REGEX = "\\d*\\.?\\d*";
    private static String VALID_SEATS_REGEX = "\\d*";
    private static String VALID_WHEELS_REGEX = "\\d*";
    private static String VALID_VIN_NUMBER_REGEX = "\\d{14}";
    private static String VALID_MASS_REGEX = "\\d*\\.?\\d*";

    public static void validate(Vehicle vehicle) throws VehicleValidationException {
        validateWithRegularExpression(vehicle.getEngineCapacity(), VALID_ENGINE_CAPACITY_REGEX, "engineCapacity", "");
        validateWithRegularExpression(vehicle.getSeats(), VALID_SEATS_REGEX, "seats", "");
        validateWithRegularExpression(vehicle.getWheels(), VALID_WHEELS_REGEX, "wheels", "");
        validateWithRegularExpression(vehicle.getVinNumber(), VALID_VIN_NUMBER_REGEX, "vinNumber", "");
        validateWithRegularExpression(vehicle.getMass(), VALID_MASS_REGEX, "mass", "");
        validateNotEmptyProperty(vehicle.getColor(), "color");
        validateNotEmptyProperty(vehicle.getModel(), "model");

    }

    private static void validateNotEmptyProperty(Object property, String propertyName) {
        if (property == null || StringUtils.isEmpty(property)) {
            throw new VehicleValidationException(String.format(EMPTY_PROPERTY_EXCEPTION_MESSAGE, propertyName));
        }
    }

    private static void validateWithRegularExpression(Object property, String regex, String propertyName, String exceptionMessage) {
        Matcher matcher = Pattern.compile(regex).matcher(String.valueOf(property));
        if (!matcher.matches()) {
            throw new VehicleValidationException(String.format(REGEX_EXCEPTION_MESSAGE, propertyName, exceptionMessage));
        }
    }
}
