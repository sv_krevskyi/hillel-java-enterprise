package com.example.data.service;

import com.example.data.entity.Manufacturer;

import java.util.List;

public interface ManufacturerService {

    Manufacturer save(Manufacturer manufacturer);

    Manufacturer findById(Long id);

    List<Manufacturer> findAll();

    void updateById(Long id, Manufacturer manufacturer);

    void delete(Long id);

}
