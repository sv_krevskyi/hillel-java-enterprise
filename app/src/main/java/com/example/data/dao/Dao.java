package com.example.data.dao;

import java.util.List;

public interface Dao<T> {

    T save(T object);

    T findById(Long id);

    List findAll();

    void updateById(Long id, T object);

    void update(T object);

    void deleteById(Long id);

    void delete(T object);

}
