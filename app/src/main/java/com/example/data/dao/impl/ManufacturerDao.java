package com.example.data.dao.impl;

import com.example.data.dao.Dao;
import com.example.data.entity.Manufacturer;
import com.example.exception.CustomDaoException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.util.List;

@Slf4j
@Repository("manufacturerDao")
public class ManufacturerDao implements Dao<Manufacturer> {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Manufacturer save(Manufacturer object) {
        return entityManager.merge(object);
    }

    @Override
    public Manufacturer findById(Long id) {
        return entityManager.find(Manufacturer.class, id);
    }

    @Override
    public List findAll() {
        return entityManager.createQuery("select m from Manufacturer m")
                .getResultList();
    }

    @Override
    public void updateById(Long id, Manufacturer object) {

    }

    @Override
    public void update(Manufacturer object) {
        entityManager.merge(object);
    }

    @Override
    public void deleteById(Long id) {
        entityManager.createQuery("delete from Manufacturer m where m.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void delete(Manufacturer manufacturer) {
        entityManager.remove(manufacturer);
    }

}
