package com.example.data.dao.impl;

import com.example.data.dao.Dao;
import com.example.data.entity.Vehicle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Slf4j
@Repository("vehicleDao")
public class VehicleDao implements Dao<Vehicle> {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Vehicle save(Vehicle object) {
        return entityManager.merge(object);
    }

    @Override
    public Vehicle findById(Long id) {
        return (Vehicle)
                entityManager.createQuery("SELECT v from Vehicle v where v.id = :id")
                        .setParameter("id", id)
                        .getSingleResult();
    }

    @Override
    public List findAll() {
        return entityManager.createQuery("select v from Vehicle v")
                .getResultList();
    }

    @Override
    public void updateById(Long id, Vehicle object) {
        Vehicle vehicle = entityManager.find(Vehicle.class, id);
        vehicle.setEngineCapacity(object.getEngineCapacity());
        vehicle.setSeats(object.getSeats());
        vehicle.setWheels(object.getWheels());
        vehicle.setVinNumber(object.getVinNumber());
        vehicle.setMass(object.getMass());
        vehicle.setColor(object.getColor());
        vehicle.setModel(object.getModel());
//        entityManager.merge(vehicle);
    }

    @Override
    public void update(Vehicle object) {
        //noop
    }

    @Override
    public void deleteById(Long id) {
        entityManager.createQuery("DELETE from Vehicle v where v.id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    public void delete(Vehicle vehicle) {
        entityManager.remove(vehicle);
    }

}
