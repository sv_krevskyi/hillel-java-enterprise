package com.example.data.dao;

import com.example.data.entity.Singer;

import java.util.List;

public interface SingerDao {
    List findAll();

    Singer findById(Long id);

    Singer save(Singer singer);

    void delete(Singer singer);

    void update(Singer singer);
}
