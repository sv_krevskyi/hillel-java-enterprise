package com.example.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@ComponentScan("com.example")
@EnableTransactionManagement
public class ProjectConfiguration {
}
