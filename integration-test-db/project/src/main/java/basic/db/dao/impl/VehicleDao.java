package basic.db.dao.impl;


import basic.db.dao.BaseDao;
import basic.db.entity.Vehicle;
import basic.exception.DAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static basic.db.dao.impl.VehicleDao.Table.COLOR;
import static basic.db.dao.impl.VehicleDao.Table.DELETE_SQL;
import static basic.db.dao.impl.VehicleDao.Table.INSERT_SQL;
import static basic.db.dao.impl.VehicleDao.Table.MANUFACTURER_ID;
import static basic.db.dao.impl.VehicleDao.Table.SEATS;
import static basic.db.dao.impl.VehicleDao.Table.SELECT_ALL_SQL;
import static basic.db.dao.impl.VehicleDao.Table.SELECT_ONE_SQL;
import static basic.db.dao.impl.VehicleDao.Table.UPDATE_SQL;
import static basic.db.dao.impl.VehicleDao.Table.VEHICLE_ID;
import static basic.db.dao.impl.VehicleDao.Table.WHEELS;

@Component
public class VehicleDao implements BaseDao<Vehicle> {
    private static final Logger LOGGER = LoggerFactory.getLogger(VehicleDao.class);
    private static final String MAIN_EXCEPTION_MESSAGE = "Exception occurred";

    private DataSource dataSource;

    @Autowired
    public VehicleDao(final DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public Vehicle create(Vehicle vehicle) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(INSERT_SQL, new String[]{VEHICLE_ID})) {

            ps.setString(1, vehicle.getColor());
            ps.setInt(2, vehicle.getWheels());
            ps.setInt(3, vehicle.getSeats());

            if (Objects.nonNull(vehicle.getManufacturerId())) { //IMPORTANT
                ps.setLong(4, vehicle.getManufacturerId());
            }

            int affectedRows = ps.executeUpdate();

            if (affectedRows == 0) {
                throw new DAOException("Error creating vehicle");
            }
            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {

                if (generatedKeys.next()) {
                    final long id = generatedKeys.getLong(1);
                    return new Vehicle(id, vehicle);

                } else {
                    throw new DAOException("Could not create vehicle");
                }
            }
        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public void create(List<Vehicle> list) {

        Connection conn = null;
        PreparedStatement ps = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false); //true

            ps = conn.prepareStatement(INSERT_SQL, new String[]{VEHICLE_ID});

            for (int i = 0; i < list.size(); i++) {

                ps.setString(1, list.get(i).getColor());
                ps.setInt(2, list.get(i).getWheels());
                ps.setInt(3, list.get(i).getSeats());
                ps.setLong(4, list.get(i).getManufacturerId());

                ps.addBatch();
            }
            ps.executeBatch();
            conn.commit();

            LOGGER.info("Prepared statement batch insert successfully");

        } catch (SQLException e) {
            try {
                conn.rollback();
            } catch (SQLException ex) {
                LOGGER.error("Error during rollback ", ex);
            }
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                LOGGER.error("Error during closing connection", e);
            }
        }
    }

    @Override
    public Vehicle get(long id) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(SELECT_ONE_SQL)) {
            ps.setLong(1, id);

            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return toVehicle(rs);
                } else {
                    return null;
                }
            }
        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public List<Vehicle> getAll() {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement ps = conn.prepareStatement(SELECT_ALL_SQL);
             ResultSet rs = ps.executeQuery()) {

            final List<Vehicle> vehicles = new ArrayList<>();
            while (rs.next()) {
                vehicles.add(toVehicle(rs));
            }
            return vehicles;
        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public boolean update(Vehicle vehicle) {
        try (Connection conn = dataSource.getConnection();

             PreparedStatement ps = conn.prepareStatement(UPDATE_SQL)) {

            ps.setString(1, vehicle.getColor());
            ps.setInt(2, vehicle.getWheels());
            ps.setInt(3, vehicle.getSeats());
            ps.setLong(4, vehicle.getManufacturerId());

            ps.setLong(5, vehicle.getVehicleId());

            final int rowCount = ps.executeUpdate();
            return rowCount != 0;

        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    @Override
    public boolean delete(long id) {
        try (Connection conn = dataSource.getConnection();
             PreparedStatement ps = conn.prepareStatement(DELETE_SQL)) {
            ps.setLong(1, id);

            final int rowCount = ps.executeUpdate();
            return rowCount != 0;

        } catch (SQLException e) {
            LOGGER.error(MAIN_EXCEPTION_MESSAGE, e);
            throw new DAOException(e);
        }
    }

    private Vehicle toVehicle(ResultSet rs) throws SQLException {
        return Vehicle.builder()
                .vehicleId(rs.getLong(VEHICLE_ID))
                .color(rs.getString(COLOR))
                .wheels(rs.getInt(WHEELS))
                .seats(rs.getInt(SEATS))
                .manufacturerId(rs.getLong(MANUFACTURER_ID))
                .build();
    }

    interface Table {
        //table
        String VEHICLE_ID = "VEHICLE_ID";
        String COLOR = "COLOR";
        String WHEELS = "WHEELS";
        String SEATS = "SEATS";
        String MANUFACTURER_ID = "MANUFACTURER_ID";

        //scripts
        String INSERT_SQL = "INSERT INTO VEHICLE (COLOR, WHEELS, SEATS, MANUFACTURER_ID) VALUES (?, ?, ?, ?)";
        String UPDATE_SQL = "UPDATE VEHICLE SET COLOR=?,WHEELS=?,SEATS=?, MANUFACTURER_ID=? WHERE VEHICLE_ID = ?";
        String SELECT_ALL_SQL = "SELECT * FROM VEHICLE";
        String SELECT_ONE_SQL = "SELECT * FROM VEHICLE WHERE VEHICLE_ID = ?";
        String DELETE_SQL = "DELETE FROM VEHICLE WHERE VEHICLE_ID = ?";
    }

}
