package basic.service;

import basic.db.entity.Manufacturer;
import basic.db.entity.Vehicle;

import java.util.List;

public interface VehicleService {
    Vehicle createVehicle(Vehicle vehicle);

    void createVehicles(List<Vehicle> vehicles);

    Vehicle getVehicleById(long id);

    List<Vehicle> getAll();

    boolean update(Vehicle vehicle);

    void assignManufacturerToVehicle(Manufacturer manufacturer, Vehicle vehicle);

    boolean delete(Vehicle vehicle);
}
