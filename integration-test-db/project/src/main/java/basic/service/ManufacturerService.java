package basic.service;

import basic.db.entity.Manufacturer;
import basic.db.entity.Vehicle;

import java.util.List;

public interface ManufacturerService {

    Manufacturer createManufacturer(Manufacturer manufacturer);

    Manufacturer createManufacturer(Manufacturer manufacturer, List<Vehicle> vehicles);

    List<Vehicle> getAllVehiclesByManufacturer(Manufacturer manufacturer);

    void createManufacturers(List<Manufacturer> manufacturers);

    Manufacturer getManufacturerById(long id);

    List<Manufacturer> getAll();

    boolean update(Manufacturer manufacturer);

    boolean delete(Manufacturer manufacturer);
}
