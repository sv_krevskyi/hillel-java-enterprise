package integration.basic;

import basic.configuration.ProjectConfiguration;
import basic.db.entity.Vehicle;
import basic.service.VehicleService;
import integration.basic.configuration.TestDatabaseConfiguration;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestDatabaseConfiguration.class, ProjectConfiguration.class})
@TestPropertySource("classpath:test.db.properties")
public class VehicleServiceIntegrationTest {

    @Autowired
    private VehicleService vehicleService;

    @Test
    @Sql(scripts = "classpath:schema.sql")
    void insertedVehicleShouldBeSavedAndHaveCorrectIdTest() {
        Vehicle actual = vehicleService.createVehicle(buildTestVehicle());
        Vehicle expected = vehicleService.getVehicleById(1l);

        assertEquals(expected, actual);
    }

    @Test
    @SqlGroup({
            @Sql(scripts = "classpath:schema.sql"),
            @Sql(statements = "INSERT INTO VEHICLE (VEHICLE_ID, COLOR, WHEELS, SEATS, MANUFACTURER_ID) VALUES (111, 'Blue', 2, 2, 10)")
    })
    void vehicleShouldBeGetByIdTest() {
        Vehicle expected = Vehicle.builder().color("Blue").vehicleId(111l).seats(2).wheels(2).manufacturerId(10l).build();
        Vehicle actual = vehicleService.getVehicleById(111l);

        assertEquals(expected, actual);
    }

    private Vehicle buildTestVehicle() {
        return Vehicle.builder().color("Red").seats(4).wheels(4).manufacturerId(10L).build();
    }
}
