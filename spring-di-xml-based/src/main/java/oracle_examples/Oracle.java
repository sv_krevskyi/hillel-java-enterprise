package oracle_examples;

import org.springframework.context.annotation.Bean;


public interface Oracle {
    String defineMeaningOfLife();
}
