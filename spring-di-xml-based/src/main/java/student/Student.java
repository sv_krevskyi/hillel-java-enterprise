package student;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Student {
    private String name;
    private int age;

    private Teacher teacher;
    private Map<String, Integer> marksMap;
    private List<Subject> subjects;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void study() {
        System.out.println("I study with teacher " + teacher.getName() + " these subjects " +
                subjects.stream().map(subject -> subject.getName()).collect(Collectors.joining(", ")) +
                " and have these marks " + marksMap.toString());
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public void setMarksMap(Map<String, Integer> marksMap) {
        this.marksMap = marksMap;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }
}
