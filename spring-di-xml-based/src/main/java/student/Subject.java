package student;

public interface Subject {
    String getName();
}
