package student;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring/Student/app-context.xml");
        Student student = (Student) context.getBean("vasya");
        student.study();
    }
}
