package second.instantiation;

public class Singer {
    private String name;

    public Singer(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
