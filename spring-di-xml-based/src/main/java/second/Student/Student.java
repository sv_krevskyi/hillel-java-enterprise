package second.Student;

import fields.InjectSimple;
import org.springframework.context.support.GenericXmlApplicationContext;

import java.util.List;
import java.util.Map;

public class Student {

    private Teacher teacher;
    private String name;
    private int age;
    private List<Subject> subjectList;
    private Map<String, Integer> subjectsGrades;

    public static void main(String... args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:spring/Student/app-context.xml");
        ctx.refresh();

        Student bean = (Student) ctx.getBeansOfType(Student.class);
        bean.study();

        ctx.close();
    }



    public void study(){
        String message = "My name is " + name + "I learn from " + teacher;
        System.out.println(message);
//        I learn {subjects} from {teacher} and have {grades}
    }

    public void setSubjectsGrades( Map subjectsGrades ) {
        this.subjectsGrades = subjectsGrades;
    }

    public Map getSubjectsGrades( ) {
        return subjectsGrades;
    }

    public void setSubjectList( List subjectList ) {
        this.subjectList = subjectList;
    }
}
