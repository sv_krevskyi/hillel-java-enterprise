package destroy;

import org.springframework.context.support.GenericXmlApplicationContext;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;

public class DestructiveWithPreDestroy {
    private File file;
    private String filePath;

    @PostConstruct
    public void afterPropertiesSet() throws Exception {
        System.out.println("Initializing  Bean");
        if (filePath == null) {
            throw new IllegalArgumentException("You must specify the filePath property of " + DestructiveWithPreDestroy.class);
        }
        this.file = new File(filePath);
        this.file.createNewFile();
        System.out.println("File exists: " + file.exists());
    }

    @PreDestroy
    public void destroy() {
        System.out.println("Destroying  Bean");
        if (!file.delete()) {
            System.err.println("ERROR: failed  to delete file.");
        }
        System.out.println("File exists: " + file.exists());
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public static void main(String... args) throws Exception {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:spring/destroy-app-context-3.xml");
        ctx.refresh();
        DestructiveWithPreDestroy bean = (DestructiveWithPreDestroy) ctx.getBean("destructiveBean");

        System.out.println("Calling destroy()");
        ctx.close(); //bruh
        System.out.println("Called destroy()");
    }

}
