package destroy.shutdownhook;

import destroy.DestructiveBean;
import org.springframework.context.support.GenericXmlApplicationContext;

public class Main {


    public static void main(String... args) throws Exception {
        GenericXmlApplicationContext ctx =
                new GenericXmlApplicationContext();
        ctx.load("classpath:spring/destroy-app-context.xml");
        ctx.refresh();
        ctx.registerShutdownHook();

        DestructiveBean bean = (DestructiveBean) ctx.getBean("destructiveBean");
    }
}
