package my_task.file_database;

import my_task.DAO;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.io.*;

public class FileDatabaseDAO implements DAO {

    private String filePath;
    private File file;

    @Override
    public void save(String string) {
        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.append(string);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getAll() {
        StringBuilder result = new StringBuilder();
        try (FileReader fileReader = new FileReader(file)) {
            int i;
            while ((i = fileReader.read()) != -1) {
                result.append((char) i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    @Value("${filePath}")
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @PostConstruct
    private void init() {
        this.file = new File(filePath + "fileDB.txt");
        try {
            file.createNewFile();
        } catch (IOException e) {
            if (file.exists()) {
                destroy();
            }
            e.printStackTrace();
        }
    }

    private void destroy() {
        if (file.delete()) {
            //success
        } else
            System.out.println("Cant delete file");
    }

}
