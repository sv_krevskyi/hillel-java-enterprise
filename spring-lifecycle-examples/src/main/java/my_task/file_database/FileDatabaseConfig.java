package my_task.file_database;

//<<<<<<< HEAD

import my_task.DAO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("/my_task.properties")
@Profile("file")
public class FileDatabaseConfig {
    @Bean
    DAO dao() {
        return new FileDatabaseDAO();
    }
}
