package my_task;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@org.springframework.stereotype.Service
public class Service {

    private DAO dao;

    @Autowired
    public void Service(DAO dao) {
        this.dao = dao;
    }

    public void save(String string) {
        dao.save(string);
    }

    public String readAll() {
        return dao.getAll();
    }
}
