package my_task.in_memory;

import my_task.DAO;
import my_task.Service;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("memory")
public class InMemoryDataBaseConfig {

    @Bean
    DAO dao() {
        return new InMemoryDataBaseDAO();
    }
}
