package my_task.in_memory;

import my_task.DAO;
//<<<<<<< HEAD
import org.springframework.stereotype.Component;

import java.util.*;
//=======

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
//>>>>>>> 586f1f99989922e36b35edd1f468d4956418ab96
import java.util.stream.Collectors;

public class InMemoryDataBaseDAO implements DAO {

    private Set<String> set = new LinkedHashSet<>();

    @Override
    public void save(String string) {
        set = Arrays.stream(string.split(",")).collect(Collectors.toSet());
        System.out.println(string + "\nSaved in memory");
    }

    @Override
    public String getAll() {
        String string = set.stream().collect(Collectors.joining(","));
        return string;
    }
}
