package my_task;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.GenericApplicationContext;

import java.util.List;

@ComponentScan(basePackages = "my_task")
public class Main {
    public static void main(String[] args) {
        GenericApplicationContext context = new AnnotationConfigApplicationContext(my_task.Main.class);

        Service service = context.getBean("service", Service.class);
        service.save("one,two,three");
        String string = service.readAll();
        System.out.println(string);
    }
}
