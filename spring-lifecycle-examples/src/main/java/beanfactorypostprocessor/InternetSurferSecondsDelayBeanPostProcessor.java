package beanfactorypostprocessor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
@Component
public class InternetSurferSecondsDelayBeanPostProcessor implements BeanPostProcessor {
    @Override
    public Object postProcessBeforeInitialization( Object bean, String beanName ) throws BeansException {
        Class<?> clazz = bean.getClass();
        Field[] declaredFields = clazz.getDeclaredFields();

        for (Field f : declaredFields) {
            SecondDelay annotation = f.getAnnotation(SecondDelay.class);
            if (annotation != null) {
                int value = annotation.value();
                f.setAccessible(true);
                ReflectionUtils.setField(f, bean, value);
            }
        }
        return bean;
    }
}
