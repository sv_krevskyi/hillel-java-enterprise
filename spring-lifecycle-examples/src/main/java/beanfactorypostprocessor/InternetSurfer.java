package beanfactorypostprocessor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

@Component
public class InternetSurfer {

    @Value("${url}")
    private String url;

    @PingTimes(4)
    private int times;

    @SecondDelay(6)
    private int secondsDelay;

    void pingUrl( ) throws IOException {
        System.out.println(String.format("Pinging %d times", times));
        for (int i = 0; i < times; i++) {
            System.out.println(String.format("PINGING %s", this.url));
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestMethod("HEAD");

            int responseCode = connection.getResponseCode();
            System.out.println(responseCode);

            try {
                Thread.sleep(1000 * secondsDelay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
