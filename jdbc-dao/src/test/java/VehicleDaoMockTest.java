import advanced.db.dao.impl.VehicleDao;
import advanced.db.entity.Vehicle;
import advanced.exception.DAOException;
import net.bytebuddy.implementation.bytecode.Throw;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mockito;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class VehicleDaoMockTest {
    //table

    private static final String VEHICLE_ID = "VEHICLE_ID";
    private static final String COLOR = "COLOR";
    private static final String WHEELS = "WHEELS";
    private static final String SEATS = "SEATS";

    //scripts

    private static final String INSERT_SQL = "INSERT INTO VEHICLE (COLOR, WHEELS, SEATS) VALUES (?,?,? )";

    private static final String UPDATE_SQL = "UPDATE VEHICLE SET COLOR=?,WHEELS=?,SEATS=? WHERE VEHICLE_ID = ?";
    private static final String SELECT_ALL_SQL = "SELECT * FROM VEHICLE";
    private static final String SELECT_ONE_SQL = "SELECT * FROM VEHICLE WHERE VEHICLE_ID = ?";
    private static final String DELETE_SQL = "DELETE FROM VEHICLE WHERE VEHICLE_ID = ?";

    //expected
    private static final long TEST_ID = 1L;
    private static final Vehicle TEST_VEHICLE_EXPECTED = new Vehicle(TEST_ID, "Red", 4, 4);

    private DataSource mockDataSource = mock(DataSource.class);
    private Connection mockConnection = mock(Connection.class);
    private PreparedStatement mockPreparedStatement = mock(PreparedStatement.class);
    private ResultSet mockResultSet = mock(ResultSet.class);

    private VehicleDao vehicleDao = new VehicleDao(mockDataSource);

    @BeforeEach
    public void before() throws Exception {
        when(mockDataSource.getConnection()).thenReturn(mockConnection);
        when(mockPreparedStatement.executeQuery()).thenReturn(mockResultSet);
        when(mockPreparedStatement.getGeneratedKeys()).thenReturn(mockResultSet);
    }

    @Test
    void whenCreateVehicleShouldReturnVehicle() throws Exception {
        //given
        when(mockConnection.prepareStatement(INSERT_SQL, new String[]{VEHICLE_ID})).thenReturn(mockPreparedStatement);
        when(mockResultSet.next()).thenReturn(true);
        when(mockPreparedStatement.executeUpdate()).thenReturn(1);

        when(mockResultSet.getLong(1)).thenReturn(TEST_ID); //*

        //when
        Vehicle actual = vehicleDao.create(buildTestVehicle());

        //then
        assertEquals(TEST_VEHICLE_EXPECTED, actual);
    }

    @Test
    void whenCreateVehicleShouldThrowExceptionIfIdWasNotGenerated() throws Exception {
        //given
        when(mockConnection.prepareStatement(INSERT_SQL, new String[]{VEHICLE_ID})).thenReturn(mockPreparedStatement);

        //when
        final Executable executable = () -> vehicleDao.create(buildTestVehicle());

        //then
        assertThrows(DAOException.class, executable);
    }

    @Test
    void whenCreateVehicleShouldThrowExceptionIfResultSetIsEmpty() throws Exception {
        //given
        when(mockConnection.prepareStatement(INSERT_SQL, new String[]{VEHICLE_ID})).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeUpdate()).thenReturn(1);
        when(mockResultSet.next()).thenReturn(false);

        //when
        final Executable executable = () -> vehicleDao.create(buildTestVehicle());

        //then
        assertThrows(DAOException.class, executable);
    }

    @Test
    void whenCreateVehicleShouldThrowExceptionIfSQLExceptionOccurred() throws Exception {
        //given
        when(mockConnection.prepareStatement(INSERT_SQL, new String[]{VEHICLE_ID})).thenThrow(new SQLException("message"));

        //when
        final Executable executable = () -> vehicleDao.create(buildTestVehicle());

        //then
        assertThrows(DAOException.class, executable);
    }

    @Test
    void whenGetShouldReturnVehicle() throws Exception {
        //given
        when(mockConnection.prepareStatement(SELECT_ONE_SQL)).thenReturn(mockPreparedStatement);
        when(mockResultSet.next()).thenReturn(true);

        buildVehicleFromMockResultSet();

        //when
        Vehicle actual = vehicleDao.get(TEST_ID);

        //then
        assertEquals(TEST_VEHICLE_EXPECTED, actual);
    }

    @Test
    void whenGetResultSetIsEmptyShouldReturnNull() throws Exception {
        //given
        when(mockConnection.prepareStatement(SELECT_ONE_SQL)).thenReturn(mockPreparedStatement);
        when(mockResultSet.next()).thenReturn(false);

        //when
        Vehicle actual = vehicleDao.get(TEST_ID);

        //then
        assertNull(actual);
    }

    @Test
    void whenGetShouldThrowExceptionIfSQLExceptionOccurred() throws Exception {
        //given
        when(mockConnection.prepareStatement(SELECT_ONE_SQL)).thenThrow(new SQLException("message"));

        //when
        final Executable executable = () -> vehicleDao.get(TEST_ID);

        //then
        assertThrows(DAOException.class, executable);
    }

    @Test
    void whenUpdateIsSuccessfulReturnsTrue() throws Exception {
        //given
        when(mockConnection.prepareStatement(UPDATE_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeUpdate()).thenReturn(1);

        assertTrue(vehicleDao.update(TEST_VEHICLE_EXPECTED));
    }

    @Test
    void whenUpdateShouldThrowExceptionIfSQLExceptionOccurred() throws Exception {
        //given
        when(mockConnection.prepareStatement(UPDATE_SQL)).thenThrow(new SQLException("message"));

        //when
        final Executable executable = () -> vehicleDao.update(TEST_VEHICLE_EXPECTED);

        //then
        assertThrows(DAOException.class, executable);
    }

    @Test
    void whenDeleteIsSuccessfulReturnsTrue() throws Exception {
        //given
        when(mockConnection.prepareStatement(DELETE_SQL)).thenReturn(mockPreparedStatement);
        when(mockPreparedStatement.executeUpdate()).thenReturn(1);

        assertTrue(vehicleDao.delete(TEST_ID));
    }

    @Test
    void whenDeleteShouldThrowExceptionIfSQLExceptionOccurred() throws Exception {
        //given
        when(mockConnection.prepareStatement(DELETE_SQL)).thenThrow(new SQLException("message"));

        //when
        final Executable executable = () -> vehicleDao.delete(TEST_ID);

        //then
        assertThrows(DAOException.class, executable);
    }

    @Test
    void whenGetAllShouldReturnVehicleList() throws Exception {
        //given
        when(mockConnection.prepareStatement(SELECT_ALL_SQL)).thenReturn(mockPreparedStatement);
        when(mockResultSet.next()).thenReturn(true, false);

        buildVehicleFromMockResultSet();

        //when
        List<Vehicle> actual = vehicleDao.getAll();

        //then
        assertEquals(TEST_VEHICLE_EXPECTED, actual.get(0));
    }

    @Test
    void whenGetAllShouldThrowExceptionIfSQLExceptionOccurred() throws Exception {
        //given
        when(mockConnection.prepareStatement(SELECT_ALL_SQL)).thenThrow(new SQLException("message"));

        //when
        final Executable executable = () -> vehicleDao.getAll();

        //then
        assertThrows(DAOException.class, executable);
    }

    @Test
    void whenCreateVehicleListShouldCreateVehicleList() throws Exception {
        //given
        when(mockConnection.prepareStatement(INSERT_SQL, new String[]{VEHICLE_ID})).thenReturn(mockPreparedStatement);

        when(mockConnection.prepareStatement(SELECT_ALL_SQL)).thenReturn(mockPreparedStatement);
        when(mockResultSet.next()).thenReturn(true, false);

        buildVehicleFromMockResultSet();

        List<Vehicle> vehicles = new ArrayList<>();
        vehicles.add(TEST_VEHICLE_EXPECTED);

        //when
        vehicleDao.create(vehicles);

        //then
        assertEquals(vehicles, vehicleDao.getAll());
    }

    @Test
    void whenCreateVehicleListShouldNotThrowExceptionIfSQLExceptionOccurred() throws Exception {
        //given
        when(mockConnection.prepareStatement(INSERT_SQL, new String[]{VEHICLE_ID})).thenThrow(new SQLException("message"));
        Mockito.doThrow(new SQLException("message 2")).when(mockConnection).rollback();
        Mockito.doThrow(new SQLException("message 3")).when(mockConnection).close();
        List<Vehicle> vehicles = new ArrayList<>();
        vehicles.add(TEST_VEHICLE_EXPECTED);

        //when
        final Executable executable = () -> vehicleDao.create(vehicles);

        //then
        assertDoesNotThrow(executable);
    }

    private Vehicle buildTestVehicle() {
        return Vehicle.builder().color("Red").seats(4).wheels(4).build();
    }

    private void buildVehicleFromMockResultSet() throws Exception {
        when(mockResultSet.getInt(VEHICLE_ID)).thenReturn(((int) TEST_ID));
        when(mockResultSet.getString(COLOR)).thenReturn(TEST_VEHICLE_EXPECTED.getColor());
        when(mockResultSet.getInt(WHEELS)).thenReturn(TEST_VEHICLE_EXPECTED.getWheels());
        when(mockResultSet.getInt(SEATS)).thenReturn(TEST_VEHICLE_EXPECTED.getSeats());
    }
}