package advanced;

import advanced.db.connection.DataSourceFactory;
import advanced.db.dao.impl.VehicleDao;
import advanced.db.entity.Vehicle;
import org.h2.tools.RunScript;

import javax.sql.DataSource;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException, FileNotFoundException {
        DataSource h2DataSource = DataSourceFactory.H2.configuration.configure();

        RunScript.execute(h2DataSource.getConnection(),
                new FileReader("jdbc-dao/src/main/resources/create_vehicle_table.sql")); //init

        VehicleDao vehicleDao = new VehicleDao(h2DataSource);

        Vehicle vehicle = Vehicle.builder()
                .color("red")
                .seats(2)
                .wheels(4).build();

        Vehicle createdVehicle = vehicleDao.create(vehicle);
        System.out.println(createdVehicle.getVehicleId());

    }
}
