package advanced.db.connection;


import com.zaxxer.hikari.HikariDataSource;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

public enum DataSourceFactory {

    H2(() -> {
        try {
            Properties properties = loadProperties("h2.config.properties");
            HikariDataSource dataSource = new HikariDataSource();
            dataSource.setUsername(properties.getProperty("db.username"));
            dataSource.setPassword(properties.getProperty("db.password"));
            dataSource.setJdbcUrl(properties.getProperty("db.url"));
            dataSource.setMinimumIdle(2);
            dataSource.setMaximumPoolSize(5);
            return dataSource;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    });


//    MARIA(new DataSourceConfiguration() {
//        @Override
//        public DataSource configure() {
//            MariaDbPoolDataSource dataSource = new MariaDbPoolDataSource();
//            try {
//                dataSource.setUrl("jdbc:mariadb://localhost:3307/sample");
//                dataSource.setPassword("root");
//                dataSource.setUser("root");
//            } catch (SQLException e) {
//                throw new IllegalArgumentException(e);
//            }
//            return dataSource;
//        }
//    }),
//
//    POSTGRESQL(new DataSourceConfiguration() {
//        @Override
//        public DataSource configure() {
//            PGSimpleDataSource source = new PGSimpleDataSource();
//            source.setURL("jdbc:postgresql://localhost:5432/sample");
//            source.setUser("postgres");
//            source.setPassword("postgres");
//            return source;
//        }
//    }),
//
//    MYSQL(
//            new DataSourceConfiguration() {
//                @Override
//                public DataSource configure() {
//                    MysqlDataSource dataSource = new MysqlDataSource();
//                    dataSource.setURL("jdbc:mysql://localhost:3306/sample");
//                    dataSource.setPassword("root");
//                    dataSource.setUser("root");
//                    return dataSource;
//                }
//            }),
//
//    MSSQL(
//            new DataSourceConfiguration() {
//                @Override
//                public DataSource configure() {
//                    SQLServerDataSource ds = new SQLServerDataSource();
//                    ds.setUser("sa");
//                    ds.setPassword("rootRootRoot#");
//                    ds.setURL("jdbc:sqlserver://localhost:1433;databaseName=sample");
//                    return ds;
//                }
//            });

    public DataSourceConfiguration configuration;

    DataSourceFactory(DataSourceConfiguration configuration) {
        this.configuration = configuration;
    }

    private static Properties loadProperties(String propertiesFileName) throws IOException {
        Properties properties = new Properties();
        properties.load(Objects.requireNonNull(ClassLoader.getSystemResourceAsStream(propertiesFileName)));
        return properties;
    }

    public interface DataSourceConfiguration {

        DataSource configure();
    }

}