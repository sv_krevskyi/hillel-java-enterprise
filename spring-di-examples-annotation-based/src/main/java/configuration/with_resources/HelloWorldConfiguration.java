package configuration.with_resources;

import message_renderer_setter.HelloWorldMessageProvider;
import message_renderer_setter.MessageProvider;
import message_renderer_setter.MessageRenderer;
import message_renderer_setter.StandardOutMessageRenderer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@ComponentScan(basePackages = {"configuration.with_resources"})
@ImportResource(locations = {"classpath:spring/conf/app-context.xml"})
@Configuration
public class HelloWorldConfiguration {

    @Bean
    public MessageProvider provider() {
        return new HelloWorldMessageProvider();
    }

    @Bean
    public MessageRenderer renderer() {
        MessageRenderer renderer = new StandardOutMessageRenderer();
        renderer.setMessageProvider(provider());
        return renderer;
    }
}

