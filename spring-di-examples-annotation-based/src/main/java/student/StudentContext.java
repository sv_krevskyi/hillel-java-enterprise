package student;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import student.impl.Maths;
import student.impl.Physics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
public class StudentContext {
    @Bean
    Map<String, Integer> studentMarks() {
        Map<String, Integer> marks = new HashMap<>();
        marks.put("Math", 4);
        marks.put("Physic", 5);
        return marks;
    }

    @Bean
    List<Subject> courseSubjects() {
        List<Subject> subjects = new ArrayList<>();
        subjects.add(new Maths());
        subjects.add(new Physics());
        return subjects;
    }
}
