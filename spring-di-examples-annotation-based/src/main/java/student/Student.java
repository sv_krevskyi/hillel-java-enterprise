package student;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class Student {
    @Value("Ivan")
    private String name;
    @Value("21")
    private int age;

    private Teacher teacher;
    private Map<String, Integer> marksMap;
    private List<Subject> subjects;


    public void study() {
        System.out.println("My name is " + this.name + " I`m " + this.age + " and I study with teacher "
                + teacher.getName() + " these subjects " +
                subjects.stream().map(subject -> subject.getName()).collect(Collectors.joining(", ")) +
                " and have these marks " + marksMap.toString());
    }

    @Autowired
    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    @Autowired
    public void setMarksMap(Map<String, Integer> marksMap) {
        this.marksMap = marksMap;
    }

    @Autowired
    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }
}
