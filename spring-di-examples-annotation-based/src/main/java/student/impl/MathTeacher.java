package student.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import student.Teacher;

@Component
public class MathTeacher implements Teacher {

    @Value("Volkov")
    private String name;

    @Override
    public String getName() {
        return this.name;
    }
}
