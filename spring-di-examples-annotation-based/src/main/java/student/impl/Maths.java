package student.impl;

import org.springframework.stereotype.Component;
import student.Subject;

@Component
public class Maths implements Subject {

    @Override
    public String getName() {
        return "Math";
    }

}
