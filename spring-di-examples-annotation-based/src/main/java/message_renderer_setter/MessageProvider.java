package message_renderer_setter;

public interface MessageProvider {
    String getMessage();
}
