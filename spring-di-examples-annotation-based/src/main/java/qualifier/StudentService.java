package qualifier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {

    @Autowired
    @Qualifier("studentIvan")
    private Student student;

    @Autowired(required = false)
    private List<String> stringList;


    public void printStudentName() {
        System.out.println(student.getName());
    }
}
