package sandbox;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = "autowired.sandbox")
public class Main {

    public static void main(String... args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(Main.class); //entry point bean
        TrickyTarget t = ctx.getBean(TrickyTarget.class);
    }
}
