package depends_on;

import org.springframework.stereotype.Component;

@Component("lucille")
public class Guitar {
    public void sing() {
        System.out.println("Cm Eb Fm Ab Bb");
    }
}
