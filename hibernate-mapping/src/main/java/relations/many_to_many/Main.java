package relations.many_to_many;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {
    public static void main(String[] args) {

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("mapping");
        EntityManager em = entityManagerFactory.createEntityManager();

        em.getTransaction().begin();

        Category someCategory = new Category("Some Category");
        Category otherCategory = new Category("Other Category");

        Item someItem = new Item("Some Item");
        Item otherItem = new Item("Other Item");

        someCategory.getItems().add(someItem);
        someItem.getCategories().add(someCategory);

        someCategory.getItems().add(otherItem);
        otherItem.getCategories().add(someCategory);

        otherCategory.getItems().add(someItem);
        someItem.getCategories().add(otherCategory);

        em.persist(someCategory);
        em.persist(otherCategory);

        em.getTransaction().commit();
    }
}
