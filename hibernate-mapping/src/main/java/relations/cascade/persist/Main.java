package relations.cascade.persist;

import relations.bidirectional.Bid;
import relations.bidirectional.Item;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("mapping");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();

        Item tv = new Item();
        tv.setName("TV");

        entityManager.persist(tv);

        Bid bid1 = Bid.builder()
                .amount(1500.0)
                .item(tv)
                .build();

        tv.getBids().add(bid1);

        Bid bid2 = Bid.builder()
                .amount(1550.0)
                .item(tv)
                .build();
        tv.getBids().add(bid2);

        tv.getBids().add(bid2);


        entityManager.getTransaction().commit();
    }
}
