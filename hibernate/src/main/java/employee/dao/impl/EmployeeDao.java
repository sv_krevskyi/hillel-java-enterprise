package employee.dao.impl;

import employee.dao.BaseDao;
import employee.entity.Employee;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class EmployeeDao implements BaseDao<Employee> {

    private final EntityManager entityManager;

    public EmployeeDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }


    @Override
    public Employee create(Employee employee) {
        this.entityManager.persist(employee);
        return employee;
    }

    @Override
    public void create(List<Employee> list) {
        this.entityManager.persist(list);
    }

    @Override
    public Employee get(long id) {
        return this.entityManager.find(Employee.class, id);
    }

    @Override
    public List<Employee> getAll() {
        return this.entityManager.createQuery("select e from employee e").getResultList();
//        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
//        CriteriaQuery<Employee> cq = cb.createQuery(Employee.class);
//        Root<Employee> rootEntry = cq.from(Employee.class);
//        CriteriaQuery<Employee> all = cq.select(rootEntry);
//        TypedQuery<Employee> allQuery = entityManager.createQuery(all);
//        return allQuery.getResultList();
    }

    @Override
    public boolean update(Employee employee) {
        Employee result = this.entityManager.merge(employee);
        return result.equals(employee);
    }

    @Override
    public boolean delete(long id) {
        Employee employee = get(id);
        this.entityManager.remove(employee);
        return get(id) == null;
    }
}
