package demo;

import demo.entity.Vehicle;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {
    public static void main(String[] args) {

        EntityManagerFactory entityManagerFactory =
                Persistence.createEntityManagerFactory("vehicle-example");

        EntityManager em = entityManagerFactory.createEntityManager();

        Vehicle vehicle = new Vehicle();
        vehicle.setColor("Red");
        vehicle.setSeats(2);
        vehicle.setWheels(4);

        em.persist(vehicle);

        Vehicle vehicle1 = em.find(Vehicle.class, 1l);
        System.out.println(vehicle1);
        em.close();
    }
}
