package employee.dao.impl;

import employee.entity.Employee;
import org.junit.jupiter.api.Test;
import org.mockito.stubbing.Answer;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

public class EmployeeDaoMockTest {

    private static final String TEST_NAME = "Johny";
    private static final Employee TEST_EMPLOYEE_EXPECTED = new Employee(TEST_NAME, 23, 5000);

    private EntityManager mockEntityManager = mock(EntityManager.class);

    private EmployeeDao employeeDao = new EmployeeDao(mockEntityManager);


    @Test
    void whenCreateEmployeeThenReturnEmployeeTest() {
        //given
        doAnswer((Answer<Object>) invocation -> {
            Employee employee = (Employee) invocation.getArguments()[0];
            employee.setName(TEST_NAME);
            return null;
        }).when(mockEntityManager).persist(TEST_EMPLOYEE_EXPECTED);
        //when
        Employee actual = employeeDao.create(TEST_EMPLOYEE_EXPECTED);
        //then
        assertEquals(TEST_EMPLOYEE_EXPECTED, actual);
    }
}
