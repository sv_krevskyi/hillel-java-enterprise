package com.example.service.impl;

import com.example.data.dao.SingerDao;
import com.example.entity.Singer;
import com.example.service.SingerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SingerServiceImpl implements SingerService {
    private SingerDao singerDao;

    @Autowired
    public SingerServiceImpl(SingerDao singerDao) {
        this.singerDao = singerDao;
    }

    @Override
    public List<Singer> findAll() {
        return singerDao.findAll();
    }

    @Override
    public Singer findById(Long id) {
        return singerDao.findById(id);
    }

    @Override
    public void save(Singer singer) {
        this.singerDao.save(singer);
    }
}
