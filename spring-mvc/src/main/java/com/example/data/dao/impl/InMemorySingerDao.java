package com.example.data.dao.impl;

import com.example.data.dao.SingerDao;
import com.example.entity.Singer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

@Repository
public class InMemorySingerDao implements SingerDao {

    private List<Singer> singers;

    private EntityManager entityManager;

    @Autowired
    public InMemorySingerDao( EntityManager entityManager ) {
        this.entityManager = entityManager;
    }

    @PostConstruct
    void init( ) {
        Singer singer1 = Singer.builder()
                .firstName("Miles")
                .lastName("Davis")
                .description("American jazz trumpeter, bandleader, and composer")
                .birthDate(new GregorianCalendar(1926, Calendar.MAY, 26).getTime())
                .id(1l)
                .build();

        Singer singer2 = Singer.builder()
                .firstName("Nick")
                .lastName("Cave")
                .description("Australian musician, singer-songwriter, author, screenwriter, " +
                        "composer and occasional actor, best known for fronting the rock band Nick Cave and the Bad Seeds")
                .birthDate(new GregorianCalendar(1957, Calendar.SEPTEMBER, 22).getTime())
                .id(2l)
                .build();

        singers = List.of(singer1, singer2);
    }


    @Override
    public List<Singer> findAll( ) {
        return this.entityManager.createQuery("select s from singer s").getResultList();
    }

    @Override
    public Singer findById( Long id ) {
        return this.entityManager.find(Singer.class, id);
    }

    @Override
    public void save( Singer singer ) {
        this.entityManager.merge(singer);
    }

}
