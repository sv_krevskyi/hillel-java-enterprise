package com.example.data.dao;

import com.example.entity.Singer;

import java.util.List;

public interface SingerDao {
    List<Singer> findAll();

    Singer findById(Long id);

    void save(Singer singer);
}
