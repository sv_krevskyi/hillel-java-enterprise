package com.example.musicmicroservice.web.controller;

import com.example.musicmicroservice.model.Lyrics;
import com.example.musicmicroservice.model.Song;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/lyrics")
@Slf4j
public class LyricsController {

    private final RestTemplate restTemplate;

    @Value("${external.service.lyrics.url}")
    private String externalLyricsServiceUrl;

    @Autowired
    public LyricsController( RestTemplate restTemplate ) {
        this.restTemplate = restTemplate;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Song> getLyricsByArtistAndSong( @RequestParam String artist, @RequestParam String songName ) {

        log.info(String.format("Request to get lyric of a song %s - %s", artist, songName));

        String getLyricsUrl = String.format(externalLyricsServiceUrl, artist, songName);

        log.info(String.format("Final url: %s", getLyricsUrl));

        final Lyrics lyrics = restTemplate.getForObject(getLyricsUrl, Lyrics.class);

        Song song = Song.builder()
                .artist(artist)
                .songName(songName)
                .lyrics(lyrics.getLyrics())
                .build();

        return new ResponseEntity<>(song, HttpStatus.OK);
    }
}
