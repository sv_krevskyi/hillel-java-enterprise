package com.example.musicmicroservice.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Song {
    private String artist;
    private String songName;
    private String lyrics;
}
