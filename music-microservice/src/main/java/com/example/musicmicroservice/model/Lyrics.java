package com.example.musicmicroservice.model;

import lombok.Data;

@Data
public class Lyrics {
    private String lyrics;
}
