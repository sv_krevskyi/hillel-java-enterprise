package my_before;

import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

public class SimpleBeforeAdvice implements MethodBeforeAdvice {
    @Override
    public void before( Method method, Object[] objects, Object o ) throws Throwable {
        System.out.println("My name is Bond. " + method.getName());
    }
}
