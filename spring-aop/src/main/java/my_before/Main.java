package my_before;

import org.springframework.aop.framework.ProxyFactory;

public class Main {
    public static void main( String[] args ) {
        Agent bond = new Bond();

        ProxyFactory proxyFactory = new ProxyFactory();

        proxyFactory.addAdvice(new SimpleBeforeAdvice());
        proxyFactory.setTarget(bond);

        Bond proxy = (Bond) proxyFactory.getProxy();
        proxy.godSaveTheQueen();
    }
}
