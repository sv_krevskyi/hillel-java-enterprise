package before;

import org.springframework.aop.framework.ProxyFactory;

public class Main {

    public static void main(String... args) {

        Guitarist johnMayer = new Guitarist();

        ProxyFactory pf = new ProxyFactory();

        pf.addAdvice(new SimpleBeforeAdvice());
        pf.setTarget(johnMayer);

        Guitarist proxy = (Guitarist) pf.getProxy();
        proxy.sing();
    }
}
