package before.secure;

import java.util.Objects;
import java.util.Scanner;

public class SecurityManager {
    private static ThreadLocal<UserInfo> threadLocal = new ThreadLocal<>();

    public void login() {
        Scanner keyboard = new Scanner(System.in);
        System.out.println("Enter username");
        String inpUser = keyboard.nextLine();
        Objects.requireNonNull(inpUser);
        System.out.println("Enter password");
        String inpPass = keyboard.nextLine();
        Objects.requireNonNull(inpPass);
        threadLocal.set(new UserInfo(inpUser, inpPass));

    }

    public void logout() {
        threadLocal.set(null);
    }

    public UserInfo getLoggedOnUser() {
        return threadLocal.get();
    }
}
