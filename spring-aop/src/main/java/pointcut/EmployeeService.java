package pointcut;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

    private Employee employee;

    public Employee getEmployee() {
        return this.employee;
    }

    @Autowired
    public void setEmployee(Employee e) {
        this.employee = e;
    }
}

