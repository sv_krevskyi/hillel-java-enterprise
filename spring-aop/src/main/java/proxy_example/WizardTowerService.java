package proxy_example;

public class WizardTowerService {

    private WizardTower wizardTower;

    public WizardTowerService(WizardTower wizardTower) {
        this.wizardTower = wizardTower;
    }

    public void enter(Wizard wizard) {
        wizardTower.enter(wizard);
    }
}
