
package proxy_example;

public interface WizardTower {

    void enter(Wizard wizard);
}
