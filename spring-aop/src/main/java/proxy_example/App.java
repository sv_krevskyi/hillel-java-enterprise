package proxy_example;


public class App {

    public static void main(String[] args) {
        WizardTowerService wizardTowerService = new WizardTowerService(new WizardTowerProxy(new IvoryTower()));
        wizardTowerService.enter(new Wizard("Red wizard"));
        wizardTowerService.enter(new Wizard("White wizard"));
        wizardTowerService.enter(new Wizard("Black wizard"));
        wizardTowerService.enter(new Wizard("Green wizard"));
        wizardTowerService.enter(new Wizard("Brown wizard"));
    }
}
