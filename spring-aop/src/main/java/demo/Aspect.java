package demo;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.springframework.stereotype.Component;

@org.aspectj.lang.annotation.Aspect
@Component
public class Aspect {

    @Around("execution(* demo.WorkingBean.doSomeHeavyWork())")
    Object logWorkTime(ProceedingJoinPoint proceedingJoinPoint) {

        System.out.println("Starting measuring time");
        long before = System.currentTimeMillis();

        Object value = null;
        try {
            value = proceedingJoinPoint.proceed();
        } catch (Throwable e) {
            e.printStackTrace();
        }
        long after = System.currentTimeMillis() - before;
        System.out.println(String.format("Finished measuring. Time spent - %d milliseconds", after));

        return value;
    }
}
