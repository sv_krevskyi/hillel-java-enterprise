package example.web.controller;

import example.data.entity.Manufacturer;
import example.service.ManufacturerService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ManufacturerControllerMockTest extends ControllerBaseTest {

    @MockBean
    private ManufacturerService manufacturerService;

    private static final Manufacturer EXPECTED_MANUFACTURER = Manufacturer.builder()
            .address("Detroit")
            .companyName("Ford")
            .carModelName("Ford")
            .foundationYear(1903)
            .id(1L)
            .build();

    private static final Manufacturer INVALID_MANUFACTURER = Manufacturer.builder()
            .address("Detroit")
            .companyName("Ford")
            .carModelName("Ford")
            .foundationYear(0)
            .id(1L)
            .build();

    //GetAll/get

    @Test
    void whenFindAllAndListIsEmptyThenRespondOkTest() throws Exception {
        when(manufacturerService.findAll()).thenReturn(List.of());

        mockMvc.perform(get("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(status().isOk());
    }

    @Test
    void whenFindAllThenRespondOkAndReturnListTest() throws Exception {
        when(manufacturerService.findAll()).thenReturn(List.of(EXPECTED_MANUFACTURER));

        mockMvc.perform(get("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].address", is("Detroit")))
                .andExpect(jsonPath("$[0].companyName", is("Ford")))
                .andExpect(jsonPath("$[0].carModelName", is("Ford")))
                .andExpect(jsonPath("$[0].foundationYear", is(1903)))
                .andExpect(jsonPath("$[0].id", is(1))); //why not 1L ?
    }

    @Test
    void whenFindAllThrowsDataAccessExceptionThenReturnCustomDaoExceptionMessageTest() throws Exception {
        when(manufacturerService.findAll()).thenThrow(new DataAccessException("mock") {
        });

        mockMvc.perform(get("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest())
//                .andExpect(jsonPath("$.timestamp",is(System.currentTimeMillis())))
                .andExpect(jsonPath("$.statusCode", is(500)))
                .andExpect(jsonPath("$.error", is("example.exception.CustomDaoException")))
                .andExpect(jsonPath("$.message", is("DataAccessException occurred in getAll method")));
    }

    //Create/post

    @Test
    void whenSaveThenReturnCreatedTest() throws Exception {
//        doAnswer((Answer<Void>) invocation -> {System.out.println("InsideOfM<ock!!"); return null;}).when(manufacturerService).save(EXPECTED_MANUFACTURER);

        mockMvc.perform(post("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(EXPECTED_MANUFACTURER)))
                .andExpect(status().isCreated());
    }

    @Test
    void whenSaveWithIncorrectYearFormatThenThrowsValidationExceptionAndReturnExceptionMessageTest() throws Exception {
        mockMvc.perform(post("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(INVALID_MANUFACTURER)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(500)))
                .andExpect(jsonPath("$.error", is("example.exception.ManufacturerValidationException")))
                .andExpect(jsonPath("$.message", is("Manufacturer field parameter 'foundationYear' must match these parameters: 'Year must consist of four digits'")));
    }

    @Test
    void whenSaveThrowsDataAccessExceptionThenReturnCustomDaoExceptionMessageTest() throws Exception {
        doThrow(new DataAccessException("mock") {
        }).when(manufacturerService).save(EXPECTED_MANUFACTURER);

        mockMvc.perform(post("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(EXPECTED_MANUFACTURER)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(500)))
                .andExpect(jsonPath("$.error", is("example.exception.CustomDaoException")))
                .andExpect(jsonPath("$.message", is("DataAccessException occurred in create method")));
    }

    //Update/put

    @Test
    void whenUpdateByIdThenReturnOkTest() throws Exception {
        mockMvc.perform(put("/manufacturers/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(EXPECTED_MANUFACTURER)))
                .andExpect(status().isOk());
    }

    @Test
    void whenUpdateByIdWithIncorrectYearFormatThenThrowsValidationExceptionAndReturnExceptionMessageTest() throws Exception {
        mockMvc.perform(put("/manufacturers/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(INVALID_MANUFACTURER)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(500)))
                .andExpect(jsonPath("$.error", is("example.exception.ManufacturerValidationException")))
                .andExpect(jsonPath("$.message", is("Manufacturer field parameter 'foundationYear' must match these parameters: 'Year must consist of four digits'")));
    }

    @Test
    void whenUpdateByIdThrowsDataAccessExceptionThenReturnCustomDaoExceptionMessageTest() throws Exception {
        doThrow(new DataAccessException("mock") {
        }).when(manufacturerService).updateById(1L, EXPECTED_MANUFACTURER);

        mockMvc.perform(put("/manufacturers/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(EXPECTED_MANUFACTURER)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(500)))
                .andExpect(jsonPath("$.error", is("example.exception.CustomDaoException")))
                .andExpect(jsonPath("$.message", is("DataAccessException occurred in update method")));
    }

    //Delete/delete

    @Test
    void whenDeleteThenReturnOkTest() throws Exception {
        mockMvc.perform(delete("/manufacturers/1"))
                .andExpect(status().isOk());
    }

    @Test
    void whenDeleteThrowsDataAccessExceptionThenReturnCustomDaoExceptionMessageTest() throws Exception {
        doThrow(new DataAccessException("mock") {
        }).when(manufacturerService).delete(1L);

        mockMvc.perform(delete("/manufacturers/1"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(500)))
                .andExpect(jsonPath("$.error", is("example.exception.CustomDaoException")))
                .andExpect(jsonPath("$.message", is("DataAccessException occurred in delete method")));
    }

}
