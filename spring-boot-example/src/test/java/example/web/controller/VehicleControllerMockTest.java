package example.web.controller;

import example.data.entity.Manufacturer;
import example.data.entity.Vehicle;
import example.service.VehicleService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataAccessException;
import org.springframework.http.MediaType;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class VehicleControllerMockTest extends ControllerBaseTest {

    @MockBean
    private VehicleService vehicleService;

    private static final Manufacturer TEST_MANUFACTURER = Manufacturer.builder()
            .address("Detroit")
            .companyName("Ford")
            .carModelName("Ford")
            .foundationYear(1903)
            .id(1L)
            .build();

    private static final Vehicle EXPECTED_VEHICLE = Vehicle.builder()
            .manufacturer(TEST_MANUFACTURER)
            .engineCapacity(2.0)
            .seats(6)
            .wheels(4)
            .vinNumber("12345678901234")
            .mass(1456.0)
            .color("Red")
            .model("X8")
            .id(1L)
            .build();

    private static final Vehicle INVALID_VEHICLE = Vehicle.builder()
            .manufacturer(TEST_MANUFACTURER)
            .engineCapacity(2.0)
            .seats(6)
            .wheels(4)
            .vinNumber("12345678901234")
            .mass(1456.0)
            .color("Red")
            .model("")
            .build();

    //GetAll/get

    @Test
    void whenGetAllVehiclesAndListIsEmptyThenRespondOkTest() throws Exception {
        when(vehicleService.getAllVehicles()).thenReturn(List.of());

        mockMvc.perform(get("/vehicles")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(status().isOk());
    }

    @Test
    void whenGetAllVehiclesThenRespondOkAndReturnListTest() throws Exception {
        when(vehicleService.getAllVehicles()).thenReturn(List.of(EXPECTED_VEHICLE));

        mockMvc.perform(get("/vehicles")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].manufacturer", is("Ford")))
                .andExpect(jsonPath("$[0].engineCapacity", is(2.0)))
                .andExpect(jsonPath("$[0].seats", is(6)))
                .andExpect(jsonPath("$[0].wheels", is(4)))
                .andExpect(jsonPath("$[0].vinNumber", is("12345678901234")))
                .andExpect(jsonPath("$[0].mass", is(1456.0)))
                .andExpect(jsonPath("$[0].color", is("Red")))
                .andExpect(jsonPath("$[0].model", is("X8")));
    }

    @Test
    void whenGetAllVehiclesThrowsDataAccessExceptionThenReturnCustomDaoExceptionMessageTest() throws Exception {
        when(vehicleService.getAllVehicles()).thenThrow(new DataAccessException("mock") {
        });

        mockMvc.perform(get("/vehicles")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest())
//                .andExpect(jsonPath("$.timestamp",is(System.currentTimeMillis())))
                .andExpect(jsonPath("$.statusCode", is(500)))
                .andExpect(jsonPath("$.error", is("example.exception.CustomDaoException")))
                .andExpect(jsonPath("$.message", is("DataAccessException occurred in getAllVehicles method")));
    }

    //Create/post

    @Test
    void whenCreateVehicleForManufacturerThenReturnCreatedTest() throws Exception {
//        doAnswer((Answer<Void>) invocation -> {System.out.println("InsideOfM<ock!!"); return null;}).when(manufacturerService).save(EXPECTED_MANUFACTURER);

        mockMvc.perform(post("/vehicles/manufacturer/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(EXPECTED_VEHICLE)))
                .andExpect(status().isCreated());
    }

    @Test
    void whenCreateVehicleForManufacturerWithEmptyModelFieldThenThrowsValidationExceptionAndReturnExceptionMessageTest() throws Exception {
        mockMvc.perform(post("/vehicles/manufacturer/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(INVALID_VEHICLE)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(500)))
                .andExpect(jsonPath("$.error", is("example.exception.VehicleValidationException")))
                .andExpect(jsonPath("$.message", is("Vehicle field parameter 'model' must be provided")));
    }

    @Test
    void whenCreateVehicleForManufacturerThrowsDataAccessExceptionThenReturnCustomDaoExceptionMessageTest() throws Exception {
        doThrow(new DataAccessException("mock") {
        }).when(vehicleService).createVehicleForManufacturer(1L, EXPECTED_VEHICLE);

        mockMvc.perform(post("/vehicles/manufacturer/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(EXPECTED_VEHICLE)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(500)))
                .andExpect(jsonPath("$.error", is("example.exception.CustomDaoException")))
                .andExpect(jsonPath("$.message", is("DataAccessException occurred in create method")));
    }

    //Update/put

    @Test
    void whenUpdateByIdThenReturnOkTest() throws Exception {
        mockMvc.perform(put("/vehicles/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(EXPECTED_VEHICLE)))
                .andExpect(status().isOk());
    }

    @Test
    void whenUpdateByIdWithEmptyModelFieldThenThrowsValidationExceptionAndReturnExceptionMessageTest() throws Exception {
        mockMvc.perform(put("/vehicles/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(INVALID_VEHICLE)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(500)))
                .andExpect(jsonPath("$.error", is("example.exception.VehicleValidationException")))
                .andExpect(jsonPath("$.message", is("Vehicle field parameter 'model' must be provided")));
    }

    @Test
    void whenUpdateByIdThrowsDataAccessExceptionThenReturnCustomDaoExceptionMessageTest() throws Exception {
        doThrow(new DataAccessException("mock") {
        }).when(vehicleService).updateById(1L, EXPECTED_VEHICLE);

        mockMvc.perform(put("/vehicles/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(EXPECTED_VEHICLE)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(500)))
                .andExpect(jsonPath("$.error", is("example.exception.CustomDaoException")))
                .andExpect(jsonPath("$.message", is("DataAccessException occurred in update method")));
    }

    //Delete/delete

    @Test
    void whenDeleteByIdThenReturnOkTest() throws Exception {
        mockMvc.perform(delete("/vehicles/1"))
                .andExpect(status().isOk());
    }

    @Test
    void whenDeleteByIdThrowsDataAccessExceptionThenReturnCustomDaoExceptionMessageTest() throws Exception {
        doThrow(new DataAccessException("mock") {
        }).when(vehicleService).deleteById(1L);

        mockMvc.perform(delete("/vehicles/1"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(500)))
                .andExpect(jsonPath("$.error", is("example.exception.CustomDaoException")))
                .andExpect(jsonPath("$.message", is("DataAccessException occurred in delete method")));
    }
}
