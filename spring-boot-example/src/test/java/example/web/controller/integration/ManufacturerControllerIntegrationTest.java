package example.web.controller.integration;

import example.data.entity.Manufacturer;
import example.exception.NotFoundException;
import example.service.ManufacturerService;
import example.web.controller.ControllerBaseTest;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.util.List;
import java.util.Objects;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ManufacturerControllerIntegrationTest extends ControllerBaseTest {

    @Autowired
    private ManufacturerService manufacturerService;

    private static final Manufacturer EXPECTED_MANUFACTURER = Manufacturer.builder()
            .address("Detroit")
            .companyName("Ford")
            .carModelName("Ford")
            .foundationYear(1903)
            .id(1L)
            .build();

    @Test
    void whenFindAllAndListIsEmptyThenRespondOkTest() throws Exception {
        mockMvc.perform(get("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(status().isOk());
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO MANUFACTURER" +
                    "(ID, ADDRESS, CAR_MODEL_NAME, COMPANY_NAME, FOUNDATION_YEAR)" +
                    " VALUES (1, 'Berlin', 'BMW', 'BMW', 1904)")
    })
    @DisplayName("should return 'manufacturer with id 1 and HTTP 200'")
    void whenFindAllThenRespondOkAndReturnListTest() throws Exception {
        mockMvc.perform(get("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].address", is("Berlin")))
                .andExpect(jsonPath("$[0].companyName", is("BMW")))
                .andExpect(jsonPath("$[0].carModelName", is("BMW")))
                .andExpect(jsonPath("$[0].foundationYear", is(1904)))
                .andExpect(jsonPath("$[0].id", is(1))); //why not 1L ?
    }

    @Test
    @DisplayName("should return 'HTTP 201'")
    void whenSaveThenReturnCreatedTest() throws Exception {
        mockMvc.perform(post("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(EXPECTED_MANUFACTURER)))
                .andExpect(status().isCreated());

        List<Manufacturer> allManufacturers = manufacturerService.findAll();

        assertTrue(allManufacturers.size() == 1);
        Manufacturer actual = allManufacturers.get(0);

        manufacturersAssertEquals(EXPECTED_MANUFACTURER, actual);

        assertTrue(Objects.nonNull(actual.getId()) && actual.getId() > 0);
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO MANUFACTURER" +
                    "(ID, ADDRESS, CAR_MODEL_NAME, COMPANY_NAME, FOUNDATION_YEAR)" +
                    " VALUES (1, 'Berlin', 'BMW', 'BMW', 1904)")
    })
    void whenUpdateByIdThenReturnOkTest() throws Exception {
        Manufacturer expected = Manufacturer.builder()
                .address("California")
                .companyName("Tesla")
                .carModelName("Model S")
                .foundationYear(2003)
                .build();

        mockMvc.perform(put("/manufacturers/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(expected)))
                .andExpect(status().isOk());

        Manufacturer actual = manufacturerService.findById(1L);

        manufacturersAssertEquals(expected, actual);
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO MANUFACTURER" +
                    "(ID, ADDRESS, CAR_MODEL_NAME, COMPANY_NAME, FOUNDATION_YEAR)" +
                    " VALUES (1, 'Berlin', 'BMW', 'BMW', 1904)")
    })
    void whenDeleteThenReturnOkTest() throws Exception {
        mockMvc.perform(delete("/manufacturers/1"))
                .andExpect(status().isOk());

        assertThrows(NotFoundException.class, () ->manufacturerService.findById(1L));
//        assertNull(manufacturerService.findById(1L));
    }

    private void manufacturersAssertEquals(Manufacturer expected, Manufacturer actual) {
        assertEquals(expected.getAddress(), actual.getAddress());
        assertEquals(expected.getCompanyName(), actual.getCompanyName());
        assertEquals(expected.getCarModelName(), actual.getCarModelName());
        assertEquals(expected.getFoundationYear(), actual.getFoundationYear());
    }

}
