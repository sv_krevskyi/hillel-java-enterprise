package example.web.controller.integration;

import example.data.entity.Manufacturer;
import example.data.entity.Vehicle;
import example.service.VehicleService;
import example.web.controller.ControllerBaseTest;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.util.List;
import java.util.Objects;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class VehicleControllerIntegrationTest extends ControllerBaseTest {

    @Autowired
    private VehicleService vehicleService;

    private static final Manufacturer TEST_MANUFACTURER = Manufacturer.builder()
            .address("Detroit")
            .companyName("Ford")
            .carModelName("Ford")
            .foundationYear(1903)
            .id(1L)
            .build();

    private static final Vehicle EXPECTED_VEHICLE = Vehicle.builder()
            .manufacturer(TEST_MANUFACTURER)
            .engineCapacity(2.0)
            .seats(6)
            .wheels(4)
            .vinNumber("12345678901234")
            .mass(1456.0)
            .color("Red")
            .model("X8")
            .id(1L)
            .build();

    @Test
    void whenGetAllVehiclesAndListIsEmptyThenRespondOkTest() throws Exception {
        mockMvc.perform(get("/vehicles")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(status().isOk());
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO MANUFACTURER" +
                    "(ID, ADDRESS, CAR_MODEL_NAME, COMPANY_NAME, FOUNDATION_YEAR)" +
                    " VALUES (1, 'Berlin', 'BMW', 'BMW', 1904)")
    })
    @DisplayName("should return 'vehicle with manufacturer BMW and HTTP 200'")
    void whenGetAllVehiclesThenRespondOkAndReturnListTest() throws Exception {
        vehicleService.createVehicleForManufacturer(1L, EXPECTED_VEHICLE);

        mockMvc.perform(get("/vehicles")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", Matchers.hasSize(1)))
                .andExpect(jsonPath("$[0].manufacturer", is("BMW")))
                .andExpect(jsonPath("$[0].engineCapacity", is(2.0)))
                .andExpect(jsonPath("$[0].seats", is(6)))
                .andExpect(jsonPath("$[0].wheels", is(4)))
                .andExpect(jsonPath("$[0].vinNumber", is("12345678901234")))
                .andExpect(jsonPath("$[0].mass", is(1456.0)))
                .andExpect(jsonPath("$[0].color", is("Red")))
                .andExpect(jsonPath("$[0].model", is("X8")));
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO MANUFACTURER" +
                    "(ID, ADDRESS, CAR_MODEL_NAME, COMPANY_NAME, FOUNDATION_YEAR)" +
                    " VALUES (1, 'Detroit', 'Ford', 'Ford', 1903)")
    })
    @DisplayName("should return 'HTTP 201'")
    void whenCreateVehicleForManufacturerThenReturnCreatedTest() throws Exception {
        mockMvc.perform(post("/vehicles/manufacturer/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(EXPECTED_VEHICLE)))
                .andExpect(status().isCreated());

        List<Vehicle> allVehicles = vehicleService.getAllVehicles();

        assertTrue(allVehicles.size() == 1);
        Vehicle actual = allVehicles.get(0);

        vehiclesAssertEquals(EXPECTED_VEHICLE, actual);

        assertTrue(Objects.nonNull(actual.getId()) && actual.getId() > 0);
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO MANUFACTURER" +
                    "(ID, ADDRESS, CAR_MODEL_NAME, COMPANY_NAME, FOUNDATION_YEAR)" +
                    " VALUES (1, 'Detroit', 'Ford', 'Ford', 1903)")
    })
    void whenUpdateByIdThenReturnOkTest() throws Exception {
        Vehicle savedVehicle = vehicleService.save(EXPECTED_VEHICLE);
        vehiclesAssertEquals(EXPECTED_VEHICLE, savedVehicle);
        assertTrue(Objects.nonNull(savedVehicle.getId()) && savedVehicle.getId() > 0);

        Vehicle expectedUpdatedVehicle = Vehicle.builder()
                .manufacturer(TEST_MANUFACTURER)
                .engineCapacity(2.0)
                .seats(6)
                .wheels(4)
                .vinNumber("12345678901234")
                .mass(1000.0)
                .color("Blue")
                .model("Model S")
                .id(1L)
                .build();

        mockMvc.perform(put("/vehicles/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(expectedUpdatedVehicle)))
                .andExpect(status().isOk());

        List<Vehicle> allVehicles = vehicleService.getAllVehicles();
        assertTrue(allVehicles.size() == 1);
        Vehicle actualUpdatedVehicle = allVehicles.get(0);

        vehiclesAssertEquals(expectedUpdatedVehicle, actualUpdatedVehicle);
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO MANUFACTURER" +
                    "(ID, ADDRESS, CAR_MODEL_NAME, COMPANY_NAME, FOUNDATION_YEAR)" +
                    " VALUES (1, 'Detroit', 'Ford', 'Ford', 1903)")
    })
    void whenDeleteByIdThenReturnOkTest() throws Exception {
        Vehicle savedVehicle = vehicleService.save(EXPECTED_VEHICLE);
        vehiclesAssertEquals(EXPECTED_VEHICLE, savedVehicle);
        assertTrue(Objects.nonNull(savedVehicle.getId()) && savedVehicle.getId() > 0);

        mockMvc.perform(delete("/vehicles/1"))
                .andExpect(status().isOk());

        List<Vehicle> allVehicles = vehicleService.getAllVehicles();
        assertTrue(allVehicles.size() == 0);
    }

    private void vehiclesAssertEquals(Vehicle expected, Vehicle actual) {
        assertEquals(expected.getManufacturer(), actual.getManufacturer());
        assertEquals(expected.getEngineCapacity(), actual.getEngineCapacity());
        assertEquals(expected.getSeats(), actual.getSeats());
        assertEquals(expected.getWheels(), actual.getWheels());
        assertEquals(expected.getVinNumber(), actual.getVinNumber());
        assertEquals(expected.getMass(), actual.getMass());
        assertEquals(expected.getColor(), actual.getColor());
        assertEquals(expected.getModel(), actual.getModel());
    }
}
