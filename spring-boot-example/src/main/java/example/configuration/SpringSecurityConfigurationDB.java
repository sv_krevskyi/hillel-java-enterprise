package example.configuration;

import example.service.auth.UserAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SpringSecurityConfigurationDB extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserAuthenticationProvider authProvider;

    @Autowired
    public void configAuthentication( AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(authProvider);
    }
    // Secure the endpoints with HTTP Basic authentication
    @Override
    protected void configure( HttpSecurity http) throws Exception {

        http
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/vehicles/**").hasAnyAuthority("user", "admin")
                .antMatchers(HttpMethod.POST, "/vehicles/**").hasAnyAuthority("user", "admin")
                .antMatchers(HttpMethod.PUT, "/vehicles/**").hasAnyAuthority("user", "admin")
                .antMatchers(HttpMethod.DELETE, "/vehicles/**").hasAnyAuthority("user", "admin")
                .antMatchers(HttpMethod.GET, "/manufacturers/**").hasAuthority("admin")
                .antMatchers(HttpMethod.POST, "/manufacturers/**").hasAuthority("admin")
                .antMatchers(HttpMethod.PUT, "/manufacturers/**").hasAuthority("admin")
                .antMatchers(HttpMethod.PATCH, "/manufacturers/**").hasAuthority("admin")
                .antMatchers(HttpMethod.DELETE, "/manufacturers/**").hasAuthority("admin")
                .and()
                .csrf().disable()
                .formLogin().disable();
    }
}
