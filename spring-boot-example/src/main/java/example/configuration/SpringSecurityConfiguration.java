//package example.configuration;
//
//import org.springframework.context.annotation.Configuration;
//import org.springframework.http.HttpMethod;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//
//@Configuration
//public class SpringSecurityConfiguration extends WebSecurityConfigurerAdapter {
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .withUser("user").password("{noop}password").authorities("USER")
//                .and()
//                .withUser("admin").password("{noop}admin").authorities("USER", "ADMIN");
//    }
//
//    //Secure endpoints with HTTP basic authentication
//    @Override
//    protected void configure( HttpSecurity http) throws Exception {
//        http
//                //HTTP Basic authentication
//                .httpBasic()
//                .and()
//                .authorizeRequests()
//                .antMatchers(HttpMethod.GET, "/vehicles/**").hasAnyAuthority("USER", "ADMIN")
//                .antMatchers(HttpMethod.POST, "/vehicles/**").hasAnyAuthority("USER", "ADMIN")
//                .antMatchers(HttpMethod.PUT, "/vehicles/**").hasAnyAuthority("USER", "ADMIN")
//                .antMatchers(HttpMethod.DELETE, "/vehicles/**").hasAnyAuthority("USER", "ADMIN")
//                .antMatchers(HttpMethod.GET, "/manufacturers/**").hasAuthority("ADMIN")
//                .antMatchers(HttpMethod.POST, "/manufacturers/**").hasAuthority("ADMIN")
//                .antMatchers(HttpMethod.PUT, "/manufacturers/**").hasAuthority("ADMIN")
//                .antMatchers(HttpMethod.PATCH, "/manufacturers/**").hasAuthority("ADMIN")
//                .antMatchers(HttpMethod.DELETE, "/manufacturers/**").hasAuthority("ADMIN")
//                .and()
//                .csrf().disable()
//                .formLogin().disable();
//    }
//}
