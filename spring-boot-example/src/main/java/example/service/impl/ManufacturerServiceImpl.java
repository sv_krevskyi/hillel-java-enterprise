package example.service.impl;

import example.data.dao.Dao;
import example.data.entity.Manufacturer;
import example.data.entity.Vehicle;
import example.service.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class ManufacturerServiceImpl implements ManufacturerService {

    private Dao<Vehicle> vehicleDao;
    private Dao<Manufacturer> manufacturerDao;

    @Autowired
    public ManufacturerServiceImpl(Dao<Vehicle> vehicleDao, Dao<Manufacturer> manufacturerDao) {
        this.vehicleDao = vehicleDao;
        this.manufacturerDao = manufacturerDao;
    }

    @Override
    public Manufacturer save(Manufacturer manufacturer) {
        return manufacturerDao.save(manufacturer);
    }

    @Override
    public Manufacturer findById(Long id) {
        return manufacturerDao.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Manufacturer> findAll() {
        return manufacturerDao.findAll();
    }

    @Override
    public void updateById(Long id, Manufacturer manufacturer) {
        Manufacturer target = findById(id);
        target.setCompanyName(manufacturer.getCompanyName());
        target.setCarModelName(manufacturer.getCarModelName());
        target.setAddress(manufacturer.getAddress());
        target.setFoundationYear(manufacturer.getFoundationYear());
//        manufacturerDao.update(manufacturer);
    }

    @Override
    public void delete(Long id) {
        Manufacturer manufacturer = manufacturerDao.findById(id);
        for (Vehicle vehicle : manufacturer.getVehicles()) {
            vehicleDao.delete(vehicle);
        }
        manufacturerDao.delete(manufacturer);
    }

}
