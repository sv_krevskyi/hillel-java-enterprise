package example.service.impl;

import example.data.dao.Dao;
import example.data.entity.Manufacturer;
import example.data.entity.Vehicle;
import example.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class VehicleServiceImpl implements VehicleService {

    private Dao<Vehicle> vehicleDao;
    private Dao<Manufacturer> manufacturerDao;

    @Autowired
    public VehicleServiceImpl(Dao<Vehicle> vehicleDao, Dao<Manufacturer> manufacturerDao) {
        this.vehicleDao = vehicleDao;
        this.manufacturerDao = manufacturerDao;
    }

    @Override
    public Vehicle save(Vehicle vehicle) {
        return vehicleDao.save(vehicle);
    }

    @Override
    public void createVehicleForManufacturer(Long manufacturerId, Vehicle vehicle) {
        Manufacturer manufacturer = manufacturerDao.findById(manufacturerId);
        manufacturer.getVehicles().add(vehicle);
        vehicle.setManufacturer(manufacturer);
        manufacturerDao.save(manufacturer);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Vehicle> getAllVehicles() {
        return vehicleDao.findAll();
    }

    @Override
    public void updateById(Long id, Vehicle vehicle) {
        vehicleDao.updateById(id, vehicle);
    }

    @Override
    public void update(Vehicle vehicle) {
        vehicleDao.update(vehicle);
    }

    @Override
    public void deleteById(Long id) {
        vehicleDao.deleteById(id);
    }

    @Override
    public void delete(Vehicle vehicle) {
        vehicleDao.delete(vehicle);
    }
}
