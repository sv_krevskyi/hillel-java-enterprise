package example.service.impl;

import example.data.dao.UserDao;
import example.data.entity.User;
import example.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;


@Service
@Transactional
public class UserServiceImpl implements UserService {

    private UserDao userDao;

    @Autowired
    public UserServiceImpl( UserDao userDao ) {
        this.userDao = userDao;
    }

    @Override
    public Optional<User> getByUsernameAndPassword( String username, String password ) {
        return Optional.of(this.userDao.getByUsernameAndPassword(username, password));
    }
}
