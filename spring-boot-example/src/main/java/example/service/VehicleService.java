package example.service;

import example.data.entity.Vehicle;

import java.util.List;

public interface VehicleService {

    Vehicle save( Vehicle vehicle );

    void createVehicleForManufacturer( Long manufacturerId, Vehicle vehicle );

    List<Vehicle> getAllVehicles( );

    void updateById( Long id, Vehicle vehicle );

    void update( Vehicle vehicle );

    void deleteById( Long id );

    void delete( Vehicle vehicle );

}
