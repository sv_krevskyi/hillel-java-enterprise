package example.exception;

public class VehicleValidationException extends ApplicationGlobalException {
    public VehicleValidationException() {
    }

    public VehicleValidationException(String message) {
        super(message);
    }

    public VehicleValidationException(String message, Throwable cause) {
        super(message, cause);
    }
}
