package example.exception.handler;

import example.exception.ApplicationGlobalException;
import example.exception.CustomDaoException;
import example.exception.ManufacturerValidationException;
import example.exception.VehicleValidationException;
import example.exception.model.ErrorMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@Slf4j
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = {ManufacturerValidationException.class, VehicleValidationException.class, CustomDaoException.class})
    public ResponseEntity<ErrorMessage> handleException(ApplicationGlobalException exception, HttpServletRequest request) {
        var httpStatus = exception.getHttpStatus();

        log.error(String.format("Exception received, path: '%s'", request.getRequestURI(), exception));

        ErrorMessage errorMessage = ErrorMessage.builder()
                .message(exception.getMessage())
                .statusCode(httpStatus.value())
                .timestamp(System.currentTimeMillis())
                .error(exception.getClass().getName())
                .build();

        return new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
    }

}
