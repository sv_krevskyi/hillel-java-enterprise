package example.web.validation;

import example.data.entity.Manufacturer;
import example.exception.ManufacturerValidationException;
import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ManufacturerValidator {
    private static final String EMPTY_PROPERTY_EXCEPTION_MESSAGE = "Manufacturer field parameter '%s' must be provided";
    private static final String REGEX_EXCEPTION_MESSAGE = "Manufacturer field parameter '%s' must match these parameters: '%s'";

    private static String VALID_YEAR_REGEX = "^[12][0-9]{3}$";

    public static void validate(Manufacturer manufacturer) throws ManufacturerValidationException {
        validateNotEmptyProperty(manufacturer.getAddress(), "address");
        validateNotEmptyProperty(manufacturer.getCarModelName(), "carModelName");
        validateNotEmptyProperty(manufacturer.getCompanyName(), "companyName");
        validateNotEmptyProperty(manufacturer.getFoundationYear(), "foundationYear");
        validateWithRegularExpression(manufacturer.getFoundationYear(), VALID_YEAR_REGEX, "foundationYear", "Year must consist of four digits");
    }

    private static void validateNotEmptyProperty(Object value, String propertyName) {
        if (value == null || StringUtils.isEmpty(value)) {
            throw new ManufacturerValidationException(String.format(EMPTY_PROPERTY_EXCEPTION_MESSAGE, propertyName));
        }
    }

    private static void validateWithRegularExpression(Object value, String regex, String propertyName, String exceptionMessage) {
        Matcher matcher = Pattern.compile(regex).matcher(String.valueOf(value));
        if (!matcher.matches()) {
            throw new ManufacturerValidationException(String.format(REGEX_EXCEPTION_MESSAGE, propertyName, exceptionMessage));
        }
    }
}
