package example.web.controller;

import example.data.entity.Manufacturer;
import example.service.ManufacturerService;
import example.exception.CustomDaoException;
import example.web.validation.ManufacturerValidator;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/manufacturers")
@Api(value = "Operations pertaining to manufacturer lifecycle [value]", description = "Operations pertaining to manufacturer lifecycle [description]")
public class ManufacturerController {

    private final ManufacturerService manufacturerService;

    @Autowired
    public ManufacturerController(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    @ApiOperation(value = "Create new manufacturer")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created manufacturer"),
            @ApiResponse(code = 400, message = "Validation error on some fields"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    public ResponseEntity create(@RequestBody Manufacturer manufacturer) {
        ManufacturerValidator.validate(manufacturer);
        try {
            manufacturerService.save(manufacturer);
        } catch (DataAccessException ex) {
            throw new CustomDaoException("DataAccessException occurred in create method", ex);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Manufacturer>> getAll() {
        try {
            return new ResponseEntity<>(manufacturerService.findAll(), HttpStatus.OK);
        } catch (DataAccessException ex) {
            throw new CustomDaoException("DataAccessException occurred in getAll method", ex);
        }
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@PathVariable Long id, @RequestBody Manufacturer manufacturer) {
        ManufacturerValidator.validate(manufacturer);
        try {
            manufacturerService.updateById(id, manufacturer);
        } catch (DataAccessException ex) {
            throw new CustomDaoException("DataAccessException occurred in update method", ex);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        try {
            manufacturerService.delete(id);
        } catch (DataAccessException ex) {
            throw new CustomDaoException("DataAccessException occurred in delete method", ex);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
