package example.web.controller;

import example.data.entity.Vehicle;
import example.service.VehicleService;
import example.exception.CustomDaoException;
import example.web.dto.VehicleDto;
import example.web.validation.VehicleValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/vehicles")
@Slf4j
public class VehicleController {

    private final VehicleService vehicleService;

    @Autowired
    public VehicleController(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }

    @PostMapping(value = "/manufacturer/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> create(@PathVariable Long id, @RequestBody Vehicle vehicle) {
        VehicleValidator.validate(vehicle);
        try {
            vehicleService.createVehicleForManufacturer(id, vehicle);
        } catch (
                DataAccessException ex) {
            throw new CustomDaoException("DataAccessException occurred in create method", ex);
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VehicleDto>> getAllVehicles() {
        try {
            List<VehicleDto> result = vehicleService.getAllVehicles().stream()
                    .map(VehicleDto::from)
                    .collect(Collectors.toList());
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (DataAccessException ex) {
            throw new CustomDaoException("DataAccessException occurred in getAllVehicles method", ex);
        }
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> update(@PathVariable Long id, @RequestBody Vehicle vehicle) {
        VehicleValidator.validate(vehicle);
        try {
            vehicleService.updateById(id, vehicle);
        } catch (DataAccessException ex) {
            throw new CustomDaoException("DataAccessException occurred in update method", ex);
        }
        return new ResponseEntity<>(HttpStatus.OK);//NO_CONTENT
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        try {
            vehicleService.deleteById(id);
        } catch (DataAccessException ex) {
            throw new CustomDaoException("DataAccessException occurred in delete method", ex);
        }
        return new ResponseEntity<>(HttpStatus.OK);//NO_CONTENT
    }

}
