package example.data.dao;

import example.data.entity.User;

public interface UserDao {

    public User getByUsernameAndPassword( String userName, String password );
}
