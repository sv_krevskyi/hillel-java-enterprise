package example.data.dao.impl;

import example.data.dao.UserDao;
import example.data.entity.User;
import example.data.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class UserDaoImpl implements UserDao {

    private UserRepository userRepository;

    @Autowired
    public UserDaoImpl( UserRepository userRepository ) {
        this.userRepository = userRepository;
    }

    @Override
    public User getByUsernameAndPassword( String userName, String password ) {
        return this.userRepository.findByUserNameAndPassword(userName, password);
    }
}
