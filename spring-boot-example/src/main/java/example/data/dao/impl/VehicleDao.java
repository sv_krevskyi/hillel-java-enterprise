package example.data.dao.impl;

import example.data.dao.Dao;
import example.data.entity.Vehicle;
import example.data.repository.VehicleRepository;
import example.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class VehicleDao implements Dao<Vehicle> {

    private VehicleRepository vehicleRepository;

    @Autowired
    public VehicleDao( VehicleRepository vehicleRepository ) {
        this.vehicleRepository = vehicleRepository;
    }

    @Override
    public Vehicle save(Vehicle object) {
        return this.vehicleRepository.save(object);
    }

    @Override
    public Vehicle findById(Long id) {
        return this.vehicleRepository.findById(id).orElseThrow(() ->
                new NotFoundException(String.format("vehicle with id '%s' not found", id)));
    }

    @Override
    public List findAll() {
        return (ArrayList) this.vehicleRepository.findAll();
    }

    @Override
    public void updateById(Long id, Vehicle object) {
        Vehicle vehicle = findById(id);
        vehicle.setEngineCapacity(object.getEngineCapacity());
        vehicle.setSeats(object.getSeats());
        vehicle.setWheels(object.getWheels());
        vehicle.setVinNumber(object.getVinNumber());
        vehicle.setMass(object.getMass());
        vehicle.setColor(object.getColor());
        vehicle.setModel(object.getModel());
//        this.vehicleRepository.save(vehicle);
    }

    @Override
    public void update(Vehicle object) {
        this.vehicleRepository.save(object);
    }

    @Override
    public void deleteById(Long id) {
        this.vehicleRepository.deleteById(id);
    }

    @Override
    public void delete(Vehicle vehicle) {
        this.vehicleRepository.delete(vehicle);
    }

}
