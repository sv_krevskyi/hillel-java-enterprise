package example.data.dao.impl;


import example.data.dao.Dao;
import example.data.entity.Manufacturer;
import example.data.repository.ManufacturerRepository;
import example.exception.NotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class ManufacturerDao implements Dao<Manufacturer> {

    private ManufacturerRepository manufacturerRepository;

    @Autowired
    public ManufacturerDao( ManufacturerRepository manufacturerRepository ) {
        this.manufacturerRepository = manufacturerRepository;
    }

    @Override
    public Manufacturer save(Manufacturer object) {
        return this.manufacturerRepository.save(object);
    }

    @Override
    public Manufacturer findById(Long id) {
        return manufacturerRepository.findById(id).orElseThrow(() ->
                new NotFoundException(String.format("manufacturer with id '%s' not found", id)));
    }

    @Override
    public List findAll() {
        return (ArrayList) manufacturerRepository.findAll();
    }

    @Override
    public void updateById(Long id, Manufacturer object) {

    }

    @Override
    public void update(Manufacturer object) {
        this.manufacturerRepository.save(object);
    }

    @Override
    public void deleteById(Long id) {
        this.manufacturerRepository.deleteById(id);
    }

    @Override
    public void delete(Manufacturer manufacturer) {
        this.manufacturerRepository.delete(manufacturer);
    }

}
