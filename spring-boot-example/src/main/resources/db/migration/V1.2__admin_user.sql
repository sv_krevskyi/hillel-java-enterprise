create table user
(
    id        bigint       not null primary key auto_increment,
    user_name varchar(255) null,
    password  varchar(255) null,
    role      varchar(255) null CHECK (role IN ('admin','user')),
    constraint UK_user_name
        unique (user_name)
);

INSERT INTO USER (user_name, password, role) VALUES ('admin', 'admin', 'admin');
INSERT INTO USER (user_name, password, role) VALUES ('user', 'password', 'user');