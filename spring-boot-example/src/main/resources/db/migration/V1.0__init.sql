create table manufacturer
(
    id              bigint       not null        primary key,
    address         varchar(255) null,
    car_model_name  varchar(255) null,
    company_name    varchar(255) null,
    foundation_year int          null,
    constraint UK_company_name
        unique (company_name)
);


create table vehicle
(
    id              bigint       not null        primary key,
    color           varchar(255) null,
    engine_capacity double       null,
    model           varchar(255) null,
    seats           int          null,
    mass            double       null,
    VIN             varchar(255) null,
    wheels          int          null,
    manufacturer_id bigint       not null,
    constraint UK_vin_number
        unique (VIN),
    constraint FKc6y2tuc57qy6qi28dqjeerodl
        foreign key (manufacturer_id) references manufacturer (id)
);